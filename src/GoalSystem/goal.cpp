//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#include "GoalSystem/goal.h"

namespace orpheus
{
namespace goal_system
{

Goal::Goal() : _satisfaction(0.0), _satisfactionThreshold(0.99), _abortSimulation(false)
{
	
}

Goal::~Goal()
{
	
}

bool Goal::isSatisfied() const
{
    return (_satisfaction > _satisfactionThreshold);
}

void Goal::setSatisfactionThreshold(double t)
{
    _satisfactionThreshold = t;
}

void Goal::setSatisfaction(double s)
{
    _satisfaction = s;
}

double Goal::getSatisfaction() const
{
    return _satisfaction;
}

bool Goal::requestsAbortSimulation() const
{
    return _abortSimulation;
}

}
}
