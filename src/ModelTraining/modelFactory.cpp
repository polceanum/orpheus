//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

//------------------------------------------------------------------------------------------------//
// Special thanks to Fabrice Harrouet [http://www.enib.fr/~harrouet/] for help on dynamic loading //
//------------------------------------------------------------------------------------------------//

#include "ModelTraining/modelFactory.h"

#ifdef __linux__
    #include <dlfcn.h>
#elif _WIN32
    #include <Windows.h>
#endif

namespace orpheus
{
namespace model_training
{

boost::shared_ptr<ModelFactory> ModelFactory::_instance;

ModelFactory::ModelFactory() //private
{
    
}

ModelFactory::~ModelFactory()
{
    //close plugins
    #ifdef __linux__
        std::map<std::string, void*>::iterator iter;
        for (iter = _handleMap.begin(); iter != _handleMap.end(); ++iter)
        {
           dlclose(iter->second);
        }
        _handleMap.clear();
    #elif _WIN32

    #endif
}

boost::shared_ptr<ModelFactory> ModelFactory::getInstance()
{
	if (!_instance)
	{
		_instance = boost::shared_ptr<ModelFactory>(new ModelFactory());
	}
	
	return _instance;
}

boost::shared_ptr<orpheus::models::Model> ModelFactory::createModel(std::string type)
{
    #ifdef __linux__
        //check if we have a dynamic library with this name -- no ? then try to load it
        if (_handleMap.find(type) == _handleMap.end())
        {
            //try to load dynamic library with this name
            std::string libName = "./lib" + type + ".so";
        
            //void *handle=dlopen(libName.c_str(),RTLD_NOW);
            void *handle=dlopen(libName.c_str(),RTLD_LAZY);
            if(handle)
            {
                _handleMap[type] = handle;
            }
            else
            {
                std::cerr << "Failed to load lib" << type << ".so ! Reason: " << dlerror() << std::endl;
            }
        }
    
        //check if we have a dynamic library with this name -- yes ? use it !
        if (_handleMap.find(type) != _handleMap.end())
        {
            //found handle ! time to create and deliver the object
            void *sym=dlsym(_handleMap[type],"createObject");
            if(sym)
            {
                orpheus::models::Model *(*fnct)();
                fnct=reinterpret_cast<orpheus::models::Model *(*)()>(sym);
                return boost::shared_ptr<orpheus::models::Model>(fnct());
            }
            else
            {
                std::cerr << "Failed to create instance of " << type << " ! Reason: " << dlerror() << std::endl;
            }
        }
    #elif _WIN32

    #endif
    
    return boost::shared_ptr<orpheus::models::Model>(); //null
}

}
}
