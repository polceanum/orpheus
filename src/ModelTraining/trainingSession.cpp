//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#include "ModelTraining/trainingSession.h"

namespace orpheus
{
namespace model_training
{

TrainingSession::TrainingSession() : _sessionId(boost::uuids::random_generator()()), _working(false), _stepSize(0.1), _modelsInitalized(false)
{
    _currentError = std::numeric_limits<double>::max();
}

TrainingSession::~TrainingSession()
{
    destroy();
}


void TrainingSession::start()
{
    bool working = false;
	_mutex.lock();
        working = _working;
    _mutex.unlock();
    
    if (!working)
    {
        _working = true;
        _thread = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&TrainingSession::worker, shared_from_this())));
    }
    else
    {
        std::cerr << "TrainingSession: Cannot start thread: already running" << std::endl;
    }
}

void TrainingSession::stop()
{
	_mutex.lock();
		_working = false;
	_mutex.unlock();
	if (_thread)
	{
		_thread->join();
	}
}

void TrainingSession::destroy()
{
	stop();
	for (int i=0; i<_models.size(); ++i)
	{
		_models[i]->erase();
	}
}

void TrainingSession::setStepSize(double s)
{
    _mutex.lock();
        _stepSize = s;
    _mutex.unlock();
}

bool TrainingSession::isRunning()
{
    bool running = true;
    if (_mutex.try_lock())
    {
        running = _working;
        _mutex.unlock();
    }
    return running;
}

void TrainingSession::addModel(boost::shared_ptr<orpheus::models::Model> model) //not thread safe !
{
    _models.push_back(model);
}

std::vector<boost::shared_ptr<orpheus::models::Model> > TrainingSession::getModelSet()
{
    std::vector<boost::shared_ptr<orpheus::models::Model> > result;
    _mutex.lock();
    for (int i=0; i<_models.size(); ++i)
    {
        result.push_back(_models[i]->copy());
    }
    _mutex.unlock();
    
    return result;
}

boost::shared_ptr<orpheus::models::Model> TrainingSession::getModel(std::size_t i)
{
    boost::shared_ptr<orpheus::models::Model> model;
    _mutex.lock();
    model = _models[i]->copy();
    _mutex.unlock();
    return model;
}

void TrainingSession::addEvidence(boost::shared_ptr<orpheus::metamodel::MentalImage> img, const value_specifications::ValueBag& params)
{
    _mutex.lock();
        _evidenceQueue.push_back(img->copy());
        _evidenceParamsQueue.push_back(params.copy());
    _mutex.unlock();
}

double TrainingSession::getCurrentError()
{
    double err = 0.0;
    _mutex.lock();
        err = _currentError;
    _mutex.unlock();
    return err;
}

void TrainingSession::setId(boost::uuids::uuid id)
{
    _sessionId = id;
}

boost::uuids::uuid TrainingSession::getId()
{
    return _sessionId;
}

void TrainingSession::worker()
{
    if (!_modelsInitalized)
    {
        for (int i=0; i<_models.size(); ++i)
        {
            _models[i]->initialize();
        }
        _modelsInitalized = true;
    }
    
	while (true)
	{
        bool working = true;
        _mutex.lock();
            working = _working;
        _mutex.unlock();
        
		if(working)
		{
            boost::shared_ptr<orpheus::metamodel::MentalImage> imgBegin;
            boost::shared_ptr<orpheus::metamodel::MentalImage> imgEnd;
            orpheus::value_specifications::ValueBag paramsBegin;
            
            _mutex.lock();
            
            //std::cerr << "_evidenceQueue.size(): " << _evidenceQueue.size() << std::endl;
            
            if (_evidenceQueue.size() > 1)
            {
                imgBegin = _evidenceQueue[0];
                imgEnd = _evidenceQueue[1];
                paramsBegin = _evidenceParamsQueue[0];
                _evidenceQueue.erase(_evidenceQueue.begin());
                _evidenceParamsQueue.erase(_evidenceParamsQueue.begin());
            }
            _mutex.unlock();
            
            //-----------------------------------------------------------------------------------------------------------------------------//
            //TESTING !!!!!!!!!!!!!!!!!
            
            double cError = 0.0;
            
            if (imgBegin && imgEnd)
            {
                uint32_t msdiff = imgEnd->getZeroTime() - imgBegin->getZeroTime();
                double dt = msdiff/1000.0;
                
                if (dt < 0.0001) continue;
                
                boost::shared_ptr<orpheus::metamodel::MentalImage> modifiedImg = imgBegin->copy();
                
                boost::shared_ptr<orpheus::metamodel::MentalImage> remainingImg = imgBegin->plus(imgEnd->minus(modifiedImg));
                remainingImg->setZeroTime(imgEnd->getZeroTime());
                //boost::shared_ptr<orpheus::metamodel::MentalImage> remainingImg = imgEnd->copy();
                
                for (int i=0; i<_models.size(); ++i)
                {
                    _models[i]->accessPermissions().clearAll(); //testing for now
                    _models[i]->accessPermissions().grantAll(modifiedImg); //testing for now
                    
                    //while (!_mutex.try_lock()); //removed for continuity, but it's not thread safe, so it should be fixed somehow
                    _mutex.lock();
                        _models[i]->learn(imgBegin, remainingImg, paramsBegin);
                    _mutex.unlock();
                    //_models[i]->learn(imgBegin, imgEnd);

                    //evaluate
                    //cError += _models[i]->getPredictionError();
                    
                    _models[i]->step(dt, imgBegin, modifiedImg);
                    remainingImg = imgBegin->plus(imgEnd->minus(modifiedImg));
                    remainingImg->setZeroTime(imgEnd->getZeroTime());
                    //_mutex.unlock();
                }
                
                _mutex.lock();
                    _currentError = cError;
                _mutex.unlock();
                //std::cerr << "Session error: " << cError << std::endl;
            }
            //-----------------------------------------------------------------------------------------------------------------------------//
		}
		else
		{
			break; //stops thread
		}
        
        //boost::this_thread::sleep(boost::posix_time::milliseconds(5)); //a lot of time to spend... fix later
        boost::this_thread::sleep_for(boost::chrono::milliseconds(5));
	}
}

}
}
