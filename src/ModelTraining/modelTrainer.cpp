//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#include "ModelTraining/modelTrainer.h"

#include <boost/uuid/uuid_io.hpp>

#include "ValueSpecifications/valueSpecification.h"
#include "ModelTraining/modelFactory.h"

namespace orpheus
{
namespace model_training
{

boost::shared_ptr<ModelTrainer> ModelTrainer::_instance;

ModelTrainer::ModelTrainer() //private
{
    
}

ModelTrainer::~ModelTrainer()
{
    
}

std::vector<boost::shared_ptr<orpheus::models::Model> > ModelTrainer::getBestTrainedModelSet()
{
    return _sessions[0]->getModelSet(); // :)
}

void ModelTrainer::addEvidence(boost::shared_ptr<orpheus::metamodel::MentalImage> img, value_specifications::ValueBag &params)
{
    _evidenceMutex.lock();
        _evidence.push_back(img->copy());
        _evidenceParams.push_back(params.copy());
    _evidenceMutex.unlock();
}

boost::shared_ptr<ModelTrainer> ModelTrainer::getInstance()
{
	if (!_instance)
	{
		_instance = boost::shared_ptr<ModelTrainer>(new ModelTrainer());
        
        //start garbage collector
        _instance->_garbageMan = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&ModelTrainer::_collectGarbage, _instance)));
        _instance->_evidenceMan = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&ModelTrainer::_manageEvidence, _instance)));
	}
	
	return _instance;
}

void ModelTrainer::addSession(boost::shared_ptr<TrainingSession> sess)
{
    _sessionMutex.lock();
    
	for (int i=0; i<_sessions.size(); ++i)
	{
		if (_sessions[i] == sess)
		{
			std::cerr << "Trying to add already existing session, skipping." << std::endl;
			return;
		}
	}
	
    _sessions.push_back(sess);
    
    _sessionMutex.unlock();
}

void ModelTrainer::removeSession(std::size_t index)
{
    if (_sessions.size()==0) std::cerr << "Requested session removal, but no sessions exist. Crashing..." << std::endl;
    _garbageMutex.lock();
        _garbage.push_back(_sessions[index]);
    _garbageMutex.unlock();
    
	_sessions.erase(_sessions.begin()+index);
}

void ModelTrainer::removeSession(boost::shared_ptr<TrainingSession> sess)
{
    for (int i=0; i<_sessions.size(); ++i)
    {
        if (_sessions[i] == sess)
        {
            _garbageMutex.lock();
                _garbage.push_back(_sessions[i]);
            _garbageMutex.unlock();
            
            _sessions.erase(_sessions.begin()+i);
            return;
        }
    }
}

void ModelTrainer::removeSession(boost::uuids::uuid id)
{
    for (int i=0; i<_sessions.size(); ++i)
    {
        if (_sessions[i]->getId() == id)
        {
            _garbageMutex.lock();
                _garbage.push_back(_sessions[i]);
            _garbageMutex.unlock();
            
            _sessions.erase(_sessions.begin()+i);
            return;
        }
    }
}

boost::shared_ptr<TrainingSession> ModelTrainer::accessSession(std::size_t index)
{
    return _sessions[index];
}

boost::shared_ptr<TrainingSession> ModelTrainer::accessSession(boost::uuids::uuid id)
{
    for (int i=0; i<_sessions.size(); ++i)
    {
        if (_sessions[i]->getId() == id)
        {
            return _sessions[i];
        }
    }
    std::cerr << "Session " << id << " not found !" << std::endl;
    return boost::shared_ptr<TrainingSession>();
}

std::size_t ModelTrainer::size()
{
	return _sessions.size();
}

void ModelTrainer::_manageEvidence()
{
    while (true)
    {
        if (_evidenceMutex.try_lock())
        {
            if (_evidence.size() > 0)
            {
                _evidenceBuffer.push_back(_evidence[0]);
                _evidenceParamsBuffer.push_back(_evidenceParams[0]);
                _evidence.erase(_evidence.begin());
                _evidenceParams.erase(_evidenceParams.begin());
            }
            _evidenceMutex.unlock();
        }
        
        _sessionMutex.lock();
        if (_evidenceBuffer.size() > 0)
        {
            for (int i=0; i<_sessions.size(); ++i)
            {
                _sessions[i]->addEvidence(_evidenceBuffer[0], _evidenceParamsBuffer[0]);
            }
            _evidenceBuffer.erase(_evidenceBuffer.begin());
            _evidenceParamsBuffer.erase(_evidenceParamsBuffer.begin());
        }
        _sessionMutex.unlock();
        
        //boost::this_thread::sleep(boost::posix_time::milliseconds(10));
        boost::this_thread::sleep_for(boost::chrono::milliseconds(10));
    }
}

void ModelTrainer::_collectGarbage()
{
    while (true)
    {
        if (_garbageMutex.try_lock())
        {
            if (_garbage.size() > 0)
            {
                _garbage[0]->destroy();
                _garbage.erase(_garbage.begin());
            }
            _garbageMutex.unlock();
        }
        //boost::this_thread::sleep(boost::posix_time::milliseconds(10));
        boost::this_thread::sleep_for(boost::chrono::milliseconds(10));
    }
}

}
}
