//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#include "ModelTraining/modelCaching.h"
#include "ModelTraining/modelTrainer.h"

namespace orpheus
{
namespace model_training
{

boost::shared_ptr<ModelCaching> ModelCaching::_instance;

ModelCaching::ModelCaching(): //private
    _working(false)
{
    
}

ModelCaching::~ModelCaching()
{
    
}

boost::shared_ptr<ModelCaching> ModelCaching::getInstance()
{
	if (!_instance)
	{
		_instance = boost::shared_ptr<ModelCaching>(new ModelCaching());
	}
	
	return _instance;
}

void ModelCaching::start()
{
    bool working = false;
	_mutex.lock();
        working = _working;
    _mutex.unlock();
    
    if (!working)
    {
        _working = true;
        _thread = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&ModelCaching::worker, shared_from_this())));
    }
    else
    {
        std::cerr << "ModelCaching: Cannot start thread: already running" << std::endl;
    }
}

void ModelCaching::stop()
{
	_mutex.lock();
		_working = false;
	_mutex.unlock();
	if (_thread)
	{
		_thread->join();
	}
}

void ModelCaching::startCachingModel(std::size_t sessionId, std::size_t modelId, std::size_t nrOfCopies)
{
    _mutex.lock();
    for (std::size_t i=0; i<_cacheData.size(); ++i)
    {
        if (_cacheData[i]->session == sessionId && _cacheData[i]->model == modelId)
        {
            _cacheData[i]->copies = nrOfCopies;
            
            _mutex.unlock();
            return;
        }
    }
    
    boost::shared_ptr<CacheData> data = boost::shared_ptr<CacheData>(new CacheData);
    data->session = sessionId;
    data->model = modelId;
    data->copies = nrOfCopies;
    _cacheData.push_back(data);
    
    _mutex.unlock();
}

void ModelCaching::stopCachingModel(std::size_t sessionId, std::size_t modelId)
{
    _mutex.lock();
    for (std::size_t i=0; i<_cacheData.size(); ++i)
    {
        if (_cacheData[i]->session == sessionId && _cacheData[i]->model == modelId)
        {
            _cacheData.erase(_cacheData.begin()+i);
            
            _mutex.unlock();
            return;
        }
    }
    _mutex.unlock();
}

boost::shared_ptr<orpheus::models::Model> ModelCaching::getCachedModel(std::size_t sessionId, std::size_t modelId)
{
    boost::shared_ptr<orpheus::models::Model> cachedModel;
    
    _mutex.lock();
    for (std::size_t i=0; i<_cacheData.size(); ++i)
    {
        if (_cacheData[i]->session == sessionId && _cacheData[i]->model == modelId)
        {
            if (_cacheData[i]->cache.size() > 0)
            {
                cachedModel = _cacheData[i]->cache[0];
                _cacheData[i]->cache.erase(_cacheData[i]->cache.begin());
            }
            
            break;
        }
    }
    _mutex.unlock();
    
    if (!cachedModel)
    {
        cachedModel = ModelTrainer::getInstance()->accessSession(sessionId)->getModel(modelId);
        std::cerr << "Cache depleted for model " << modelId << " in session " << sessionId << ". Created new copy (slowly)." << std::endl;
    }
    
    return cachedModel;
}

void ModelCaching::worker()
{
    while (true)
	{
        bool working = true;
        _mutex.lock();
            working = _working;
        _mutex.unlock();
        
		if(working)
		{
            for (std::size_t i=0; ; ++i)
            {
                std::size_t sessionId;
                std::size_t modelId;
                std::size_t nrOfCopies;
                _mutex.lock();
                if (i >= _cacheData.size())
                {
                    _mutex.unlock();
                    break;
                }
                
                if (_cacheData[i]->cache.size() >= _cacheData[i]->copies)
                {
                    _mutex.unlock();
                    continue;
                }
                
                sessionId = _cacheData[i]->session;
                modelId = _cacheData[i]->model;
                nrOfCopies = _cacheData[i]->copies;
                
                _mutex.unlock();
                
                //copy model
                boost::shared_ptr<orpheus::models::Model> copy = ModelTrainer::getInstance()->accessSession(sessionId)->getModel(modelId);
                
                _mutex.lock();
                for (std::size_t i=0; i<_cacheData.size(); ++i)
                {
                    if (_cacheData[i]->session == sessionId && _cacheData[i]->model == modelId)
                    {
                        _cacheData[i]->cache.push_back(copy);
                        
                        break;
                    }
                }
                _mutex.unlock();
                
                boost::this_thread::sleep_for(boost::chrono::milliseconds(5));
            }
		}
		else
		{
			break; //stops thread
		}
        
        boost::this_thread::sleep_for(boost::chrono::milliseconds(5));
	}
}

}
}
