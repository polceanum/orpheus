//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#include "Metamodel/mentalImage.h"

#include "Tools/base64.h"
#include <boost/tokenizer.hpp>
#include <boost/uuid/uuid_io.hpp>         // streaming operators etc.

namespace orpheus
{
namespace metamodel
{

MentalImage::MentalImage()
{
	_zeroTime = 0; //default
    _passedMillis = 0;
}

MentalImage::~MentalImage()
{
    _objectVector.clear();
    _objectMap.clear();
    _passedMillis = 0;
}

void MentalImage::setZeroTime(uint32_t time)
{
    _zeroTime = time;
}

uint32_t MentalImage::getZeroTime() const
{
    return _zeroTime;
}

void MentalImage::advanceVirtualTime(uint32_t millis)
{
    _passedMillis += millis;
}

uint32_t MentalImage::getPassedMillis()
{
    return _passedMillis;
}

uint32_t MentalImage::getCurrentVirtualTime() const
{
    return (_zeroTime+_passedMillis);
}

void MentalImage::addObject(const MentalEntity &po) //This is not meant to handle adding the same object many times. Should probably say something if you do. You have been warned !
{
    //_objects.push_back(po);
    _objectVector.push_back(po.id);
    //_objectMap[po.id] = po;
    _objectMap.insert ( std::pair<boost::uuids::uuid, MentalEntity>(po.id, po) );
}

void MentalImage::removeObject(boost::uuids::uuid id)
{
    for (int i=0; i<_objectVector.size(); ++i)
    {
        if (_objectVector[i] == id)
        {
            _objectVector.erase(_objectVector.begin()+i);
            break;
        }
    }
    
    _objectMap.erase(id);
}

MentalEntity& MentalImage::accessObject(std::size_t index)
{
    return _objectMap.at(_objectVector[index]);
}

MentalEntity& MentalImage::accessObject(boost::uuids::uuid id)
{
    return _objectMap.at(id);
}

const MentalEntity& MentalImage::getObject(std::size_t index) const
{
    return _objectMap.at(_objectVector[index]);
}

const MentalEntity& MentalImage::getObject(boost::uuids::uuid id) const
{
    return _objectMap.at(id);
}

bool MentalImage::contains(boost::uuids::uuid id) const
{
    std::map<boost::uuids::uuid, MentalEntity>::const_iterator it;
    it = _objectMap.find(id);
    if (it != _objectMap.end())
    {
        return true;
    }
    
    return false;
}

std::size_t MentalImage::size() const
{
    return _objectVector.size();
}

boost::shared_ptr<MentalImage> MentalImage::plus(boost::shared_ptr<const MentalImage> img) const
{
    boost::shared_ptr<MentalImage> result = boost::shared_ptr<MentalImage>(new MentalImage());
    
    for (std::size_t i=0; i<_objectVector.size(); ++i)
    {
        MentalEntity obj;
        const orpheus::metamodel::MentalEntity &first = getObject(i);
        const orpheus::metamodel::MentalEntity &second = img->getObject(i);
        
        obj.pos = (first.pos+second.pos);
        obj.rot = (first.rot*second.rot).normalized();
        obj.lvel = (first.lvel+second.lvel);
        obj.avel = (first.avel+second.avel);
        
        result->addObject(obj);
    }
    
    return result;
}

boost::shared_ptr<MentalImage> MentalImage::minus(boost::shared_ptr<const MentalImage> img) const
{
    boost::shared_ptr<MentalImage> result = boost::shared_ptr<MentalImage>(new MentalImage());
    
    for (std::size_t i=0; i<_objectVector.size(); ++i)
    {
        MentalEntity obj;
        const orpheus::metamodel::MentalEntity &first = getObject(i);
        const orpheus::metamodel::MentalEntity &second = img->getObject(i);
        
        obj.pos = (first.pos-second.pos);
        obj.rot = (first.rot*(second.rot.inverse())).normalized();
        obj.lvel = (first.lvel-second.lvel);
        obj.avel = (first.avel-second.avel);
        
        result->addObject(obj);
    }
    
    return result;
}

std::string MentalImage::toString()
{
    std::string serialImg = "MentalImage";
    
    serialImg += ":"+base64_encode(boost::lexical_cast<std::string>(getZeroTime())); //TODO change the actual way this time is transmitted, now it looks smth like: 2014-Feb-14 15:06:23.269240 which is not very generic
    
    for (std::size_t i=0; i<_objectVector.size(); ++i)
    {
        serialImg += ":"+_objectMap.at(_objectVector[i]).toString();
    }
    return base64_encode(serialImg);
}

boost::shared_ptr<MentalImage> MentalImage::fromString(std::string str)
{
    boost::shared_ptr<MentalImage> pim = boost::shared_ptr<MentalImage>(new MentalImage());
    
    const std::string &decodedString = base64_decode(str);
    boost::char_separator<char> sep(":");
    boost::tokenizer<boost::char_separator<char> > tokens(decodedString, sep);
    for (boost::tokenizer<boost::char_separator<char> >::iterator tok_iter = tokens.begin(); tok_iter!=tokens.end(); ++tok_iter)
    {
        const std::string tok = *tok_iter;
        if (tok == "MentalImage")
        {
            ++tok_iter;
            
            const std::string tok2 = *tok_iter;
            pim->setZeroTime(boost::lexical_cast<uint32_t>(base64_decode(tok2)));
            
            continue;
        }
        
        MentalEntity po;
        po.fromString(tok);
        pim->_objectVector.push_back(po.id);
        pim->_objectMap[po.id] = po;
    }
    
    return pim;
}

/*
void print()
{
    std::string pr = "";
    pr += "MentalImage: " + 
}
*/

//test
void MentalImage::printDiffTo(boost::shared_ptr<const MentalImage> img) const
{
    if (_objectVector.size() != img->_objectVector.size())
    {
        std::cerr << "inequal number of objects (" << _objectVector.size() << " vs " << img->_objectVector.size() << ")" << std::endl;
    }
    else
    {
        for (std::size_t i=0; i<_objectVector.size(); ++i)
        {
            //btVector3 thisPos = _objectMap.at(_objectVector[i]).pos;
            //btVector3 thatPos = img->_objectMap.at(img->_objectVector[i]).pos;
            //btVector3 diff = thisPos-thatPos;
            
            Eigen::Vector3d thisVel = _objectMap.at(_objectVector[i]).lvel;
            Eigen::Vector3d thatVel = img->_objectMap.at(img->_objectVector[i]).lvel;
            Eigen::Vector3d diff = thisVel-thatVel;
            std::cerr << "Diff " << i << ": " << diff.x() << " " << diff.y() << " " << diff.z() << " Speed(this): " << thisVel.x() << " " << thisVel.y() << " " << thisVel.z() << std::endl;
        }
    }
}

void MentalImage::printCorrespondenceTo(boost::shared_ptr<const MentalImage> img) const
{
    size_t mySize = size();
    size_t imgSize = img->size();
    double **cMatrix = (double**)malloc(mySize*sizeof(double*));
    for (size_t i = 0; i < mySize; ++i) cMatrix[i] = (double*)malloc(imgSize*sizeof(double));
    int *match = (int*)malloc(mySize*sizeof(int));
    double *matchDist = (double*)malloc(mySize*sizeof(double));
    
    for (int i=0; i<mySize; ++i)
    {
        double minDist = -1;
        for (int j=0; j<imgSize; ++j)
        {
            cMatrix[i][j] = this->getObject(i).differenceTo(img->getObject(j));
            if ((minDist < 0) || (cMatrix[i][j] < minDist))
            {
                match[i] = j;
                minDist = cMatrix[i][j];
                matchDist[i] = cMatrix[i][j];
            }
        }
    }
    
    std::cerr << "cMatrix:" << std::endl;
    for (int i=0; i<size(); ++i)
    {
        for (int j=0; j<img->size(); ++j)
        {
            if (match[i] == j)
            {
                std::cerr << "X";
            }
            else
            {
                std::cerr << ".";
            }
        }
        std::cerr << " (" << matchDist[i] << ")" << std::endl;
    }
    std::cerr << std::endl;

    for (size_t i = 0; i < mySize; ++i) free(cMatrix[i]);
    free(cMatrix);
    free(match);
    free(matchDist);
}

std::vector<boost::uuids::uuid> MentalImage::adjustToCorrespondWith(boost::shared_ptr<const MentalImage> img)
{
    if (size() == 0) return std::vector<boost::uuids::uuid>();
    
    std::vector<int> goodIndexes;
    
    for (size_t i=0; i<img->size(); ++i)
    {
        if (!contains(img->getObject(i).id))
        {
            goodIndexes.push_back(i);
        }
    }
    
    if (goodIndexes.size() == 0) return std::vector<boost::uuids::uuid>();
    
    size_t mySize = size();
    size_t indexSize = goodIndexes.size();
    double **cMatrix = (double**)malloc(mySize*sizeof(double*));
    for (size_t i = 0; i < mySize; ++i) cMatrix[i] = (double*)malloc(indexSize*sizeof(double));
    int *match = (int*)malloc(mySize*sizeof(int));
    double *matchDist = (double*)malloc(mySize*sizeof(double));

    for (size_t i=0; i<mySize; ++i)
    {
        double minDist = -1;
        for (int j=0; j<indexSize; ++j)
        {
            cMatrix[i][j] = this->getObject(i).differenceTo(img->getObject(goodIndexes[j]));
            if ((minDist < 0) || (cMatrix[i][j] < minDist))
            {
                match[i] = j;
                minDist = cMatrix[i][j];
                matchDist[i] = cMatrix[i][j];
            }
        }
    }
    
    //mean
    double mean = 0.0;
    for (size_t i=0; i<mySize; ++i)
    {
        mean += matchDist[i];
    }
    mean /= size();
    
    //variance
    double variance = 0.0;
    for (size_t i=0; i<mySize; ++i)
    {
        double dif = matchDist[i]-mean;
        variance += dif*dif;
    }
    variance /= size();
    
    bool *duplicate = (bool*)malloc(mySize*sizeof(bool));
    for (size_t i=0; i<size(); ++i)
    {
        duplicate[i] = false;
    }
    for (size_t i=0; i<mySize-1; ++i)
    {
        for (size_t j=i+1; j<mySize; ++j)
        {
            if (match[i] == match[j])
            {
                duplicate[i] = true;
                duplicate[j] = true;
            }
        }
    }
    
    std::vector<boost::uuids::uuid> transferred;
    //standard deviation
    double sigma = sqrt(variance);
    //double threshold = mean+2*sigma;
    double threshold = mean+sigma;
    
    for (size_t i=0; i<mySize; ++i)
    {
        if (!duplicate[i] && (matchDist[i] < threshold))
        {
            boost::uuids::uuid replacement = img->getObject(goodIndexes[match[i]]).id;
            replaceObjectId(i, replacement);
            transferred.push_back(replacement);
        }
    }

    free(duplicate);
    for (size_t i = 0; i < mySize; ++i) free(cMatrix[i]);
    free(cMatrix);
    free(match);
    free(matchDist);
    
    return transferred;
}

void MentalImage::replaceObjectId(int index, boost::uuids::uuid replacement)
{
    boost::uuids::uuid origId = _objectVector[index];
    MentalEntity origObj = _objectMap[origId];
    
    origObj.id = replacement;
    
    _objectVector[index] = replacement;
    _objectMap.erase(replacement);
    _objectMap[replacement] = origObj;
}

bool MentalImage::hasDuplicates()
{
    for (int i=0; i<_objectVector.size()-1; ++i)
    {
        for (int j=i+1; j<_objectVector.size(); ++j)
        {
            if (_objectVector[i] == _objectVector[j])
            {
                return true;
            }
        }
    }
    return false;
}

boost::shared_ptr<MentalImage> MentalImage::copy() const //TODO: unnecessary copies are made when the object is returned. fix this later
{
    boost::shared_ptr<MentalImage> pim = boost::shared_ptr<MentalImage>(new MentalImage());
    pim->_zeroTime = _zeroTime;
    pim->_passedMillis = _passedMillis;
    
    pim->_objectVector = _objectVector;
    pim->_objectMap = _objectMap;
    
    return pim;
}

}
}
