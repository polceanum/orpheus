//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#include "Metamodel/mentalEntity.h"

#include <boost/lexical_cast.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/tokenizer.hpp>
#include "Tools/base64.h"

namespace orpheus
{
namespace metamodel
{

MentalEntity::MentalEntity() : id(boost::uuids::random_generator()())
{
    sh = "";
    shSize = Eigen::Vector3d(1.0, 1.0, 1.0);
    mass = 1.0;
    pos = Eigen::Vector3d(0.0, 0.0, 0.0);
    rot = Eigen::Quaterniond(1.0, 0.0, 0.0, 0.0);
    lvel = Eigen::Vector3d(0.0, 0.0, 0.0);
    avel = Eigen::Vector3d(0.0, 0.0, 0.0);
    force = Eigen::Vector3d(0.0, 0.0, 0.0);
    torque = Eigen::Vector3d(0.0, 0.0, 0.0);
}

MentalEntity::~MentalEntity()
{
    
}

std::string MentalEntity::toString()
{
    std::string serialObj = "";
    serialObj += boost::lexical_cast<std::string>(id)+":";
    serialObj += sh+":";
    serialObj += boost::lexical_cast<std::string>(shSize.x())+":"+boost::lexical_cast<std::string>(shSize.y())+":"+boost::lexical_cast<std::string>(shSize.z())+":";
    serialObj += boost::lexical_cast<std::string>(mass)+":";
    serialObj += boost::lexical_cast<std::string>(pos.x())+":"+boost::lexical_cast<std::string>(pos.y())+":"+boost::lexical_cast<std::string>(pos.z())+":";
    serialObj += boost::lexical_cast<std::string>(rot.w())+":"+boost::lexical_cast<std::string>(rot.x())+":"+boost::lexical_cast<std::string>(rot.y())+":"+boost::lexical_cast<std::string>(rot.z())+":";
    serialObj += boost::lexical_cast<std::string>(lvel.x())+":"+boost::lexical_cast<std::string>(lvel.y())+":"+boost::lexical_cast<std::string>(lvel.z())+":";
    serialObj += boost::lexical_cast<std::string>(avel.x())+":"+boost::lexical_cast<std::string>(avel.y())+":"+boost::lexical_cast<std::string>(avel.z())+":";
    serialObj += boost::lexical_cast<std::string>(force.x())+":"+boost::lexical_cast<std::string>(force.y())+":"+boost::lexical_cast<std::string>(force.z())+":";
    serialObj += boost::lexical_cast<std::string>(torque.x())+":"+boost::lexical_cast<std::string>(torque.y())+":"+boost::lexical_cast<std::string>(torque.z());
    return base64_encode(serialObj);
}

void MentalEntity::fromString(const std::string &str)
{
    const std::string &decodedString = base64_decode(str);
    //std::cerr << "((" << decodedString << "))" << std::endl;
    boost::char_separator<char> sep(":");
    boost::tokenizer<boost::char_separator<char> > tokens(decodedString, sep);
    int slot = 0;
    for (boost::tokenizer<boost::char_separator<char> >::iterator tok_iter = tokens.begin(); tok_iter!=tokens.end(); ++tok_iter)
    {
        const std::string tok = *tok_iter;
        //std::cerr << "slot: " << slot << " val: [" << tok << "]" << std::endl;
        switch(slot)
        {
            case 0:  boost::uuids::string_generator gen; id=gen(tok); break;
            case 1:  sh=(tok.c_str()); break;
            case 2:  shSize[0]=boost::lexical_cast<double>(tok.c_str()); break;
            case 3:  shSize[1]=boost::lexical_cast<double>(tok.c_str()); break;
            case 4:  shSize[2]=boost::lexical_cast<double>(tok.c_str()); break;
            case 5:  mass=boost::lexical_cast<double>(tok.c_str()); break;
            case 6:  pos[0]=boost::lexical_cast<double>(tok.c_str()); break;
            case 7:  pos[1]=boost::lexical_cast<double>(tok.c_str()); break;
            case 8:  pos[2]=boost::lexical_cast<double>(tok.c_str()); break;
            case 9:  rot.w()=boost::lexical_cast<double>(tok.c_str()); break;
            case 10: rot.x()=boost::lexical_cast<double>(tok.c_str()); break;
            case 11: rot.y()=boost::lexical_cast<double>(tok.c_str()); break;
            case 12: rot.z()=boost::lexical_cast<double>(tok.c_str()); break;
            case 13: lvel[0]=boost::lexical_cast<double>(tok.c_str()); break;
            case 14: lvel[1]=boost::lexical_cast<double>(tok.c_str()); break;
            case 15: lvel[2]=boost::lexical_cast<double>(tok.c_str()); break;
            case 16: avel[0]=boost::lexical_cast<double>(tok.c_str()); break;
            case 17: avel[1]=boost::lexical_cast<double>(tok.c_str()); break;
            case 18: avel[2]=boost::lexical_cast<double>(tok.c_str()); break;
            case 19: force[0]=boost::lexical_cast<double>(tok.c_str()); break;
            case 20: force[1]=boost::lexical_cast<double>(tok.c_str()); break;
            case 21: force[2]=boost::lexical_cast<double>(tok.c_str()); break;
            case 22: torque[0]=boost::lexical_cast<double>(tok.c_str()); break;
            case 23: torque[1]=boost::lexical_cast<double>(tok.c_str()); break;
            case 24: torque[2]=boost::lexical_cast<double>(tok.c_str()); break;
        }

        slot++;
    }
}

double MentalEntity::differenceTo(const MentalEntity & po) const
{
    /*
    double diff = 0.0;
    diff += (this->pos - po.pos).length();
    //diff += (this->rot - po.rot).length();
    diff += (this->lvel - po.lvel).length();
    //diff += (this->avel - po.avel).length();
    */
    
    double d1 = (this->pos - po.pos).norm();
    double d2 = (this->lvel - po.lvel).norm();
    
    double diff = sqrt(d1*d1 + d2*d2);
    //double diff = d1;
    
    if (this->sh != po.sh)
    {
        diff += 1000.0;
    }
    
    return diff;
}

/*
MentalEntity MentalEntity::copy()
{
    MentalEntity po;
    po.id = this->id;
    po.pos = this->pos;
    po.rot = this->rot;
    po.lvel = this->lvel;
    po.avel = this->avel;
    return po;
}
*/
}
}
