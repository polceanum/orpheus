//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#include "Models/modelPythonWrapper.h"

#include <boost/date_time/posix_time/posix_time.hpp>

namespace orpheus
{
namespace models
{

ModelPythonWrapper::ModelPythonWrapper()
{
	
}

ModelPythonWrapper::~ModelPythonWrapper()
{
	
}



boost::shared_ptr<ModelPythonWrapper::PythonContextManager> ModelPythonWrapper::PythonContextManager::_instance = boost::shared_ptr<PythonContextManager>();

boost::shared_ptr<ModelPythonWrapper::PythonContextManager> ModelPythonWrapper::PythonContextManager::getInstance()
{
    if (!_instance)
    {
        _instance = boost::shared_ptr<PythonContextManager>(new PythonContextManager());
    }
    
    return _instance;
}

ModelPythonWrapper::PythonContextManager::PythonContextManager()
{
    Py_Initialize(); //Py_InitializeEx(0);
    PyEval_InitThreads();
    
    try
    {
        _main_module = boost::python::import("__main__");
        _globalDict = boost::python::extract<boost::python::dict>(_main_module.attr("__dict__"));
        
        _state = PyEval_SaveThread();
        
        _interpreterState = _state->interp;
    }
    catch(const std::exception &e)
    {
        std::cerr << "Exception in script: " << e.what() << "\n";
    }
}

ModelPythonWrapper::PythonContextManager::~PythonContextManager()
{
    Py_Finalize();
}

boost::python::dict ModelPythonWrapper::PythonContextManager::createContextFromFile(std::string fileName)
{
    boost::python::dict localDict = _globalDict.copy();
    try
    {
        PyEval_RestoreThread(_state);
        
        try
        {
            //boost::posix_time::ptime t1 = boost::posix_time::microsec_clock::local_time();
            
            boost::python::exec_file(fileName.c_str(), localDict, localDict);
            
            //boost::posix_time::ptime t2 = boost::posix_time::microsec_clock::local_time();
            
            //boost::posix_time::time_duration msdiff = t2 - t1;
            //std::cerr << "time to load python script: " << msdiff.total_milliseconds()/1000.0f << std::endl;
        }
        catch(const boost::python::error_already_set &e)
        {
            std::cerr << "Exception in script: ";
            print_py_error();
        }
        
        _state = PyEval_SaveThread();
    }
    catch(const std::exception &e)
    {
        std::cerr << "Exception in script: " << e.what() << "\n";
    }
    
    return localDict;
}

void ModelPythonWrapper::PythonContextManager::print_py_error()
{
    try
    {
        PyErr_Print();
        boost::python::object sys(boost::python::handle<>(PyImport_ImportModule("sys")));
        boost::python::object err = sys.attr("stderr");
        std::string err_text = boost::python::extract<std::string>(err.attr("getvalue")());
        std::cerr << err_text << "\n";
    }
    catch (...)
    {
        std::cerr << "Failed to parse python error\n";
    }
    PyErr_Clear();
}

void ModelPythonWrapper::py_call(std::string func)
{
    PyExternalUser::Use use(*_pyUser);
    try
    {
        try
        {
            boost::python::object f = boost::python::extract<boost::python::object>(_context[func.c_str()]);
            if(f)
            {
                f(); //call the function !
            }
            else
            {
                std::cerr << "Script did not have a " << func << " function!\n";
            }
        }
        catch(const boost::python::error_already_set &e)
        {
            std::cerr << "Exception in script: ";
            ModelPythonWrapper::PythonContextManager::getInstance()->print_py_error();
        }
    }
    catch(const std::exception &e)
    {
        std::cerr << "Exception in script: " << e.what() << "\n";
    }
}

/*
void ModelPythonWrapper::py_call(std::string func, std::map<std::string, float> args)
{
    PyExternalUser::Use use(*_pyUser);
    try
    {
        try
        {
            boost::python::object f = boost::python::extract<boost::python::object>(_context[func.c_str()]);
            if(f)
            {
                boost::python::dict dictData;
                std::map<std::string, float>::iterator iter;
                for (iter = args.begin(); iter != args.end(); ++iter)
                {
                    dictData[iter->first] = iter->second;
                }
                
                f(dictData); //call the function !
            }
            else
            {
                std::cerr << "Script did not have a " << func << " function!\n";
            }
        }
        catch(const boost::python::error_already_set &e)
        {
            std::cerr << "Exception in script: ";
            ModelPythonWrapper::PythonContextManager::getInstance()->print_py_error();
        }
    }
    catch(const std::exception &e)
    {
        std::cerr << "Exception in script: " << e.what() << "\n";
    }
}
*/

}
}
