//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#include "Models/model.h"

#include "Metamodel/mentalImage.h"

namespace orpheus
{
namespace models
{

Model::Model()
{
	
}

Model::~Model()
{
	
}

void Model::learn(boost::shared_ptr<const orpheus::metamodel::MentalImage>, boost::shared_ptr<const orpheus::metamodel::MentalImage>, const orpheus::value_specifications::ValueBag&)
{
    //dummy
    std::cerr << "Model: called learn() on a model which doesn't implement the learn method." << std::endl;
}

double Model::getPredictionError()
{
    //dummy
    std::cerr << "Model: called getPredictionError on a model which does not provide this." << std::endl;

    return 0.0;
}

void Model::saveInternalData()
{
    //dummy
    std::cerr << "Model: called saveInternalData on a model which does not provide this." << std::endl;
}

void Model::loadInternalData()
{
    //dummy
    std::cerr << "Model: called loadInternalData on a model which does not provide this." << std::endl;
}

void Model::reset()
{
    erase();
	initialize();
}

void Model::step(double dt, boost::shared_ptr<const orpheus::metamodel::MentalImage> input, boost::shared_ptr<orpheus::metamodel::MentalImage> result)
{
    _virtualTimeBeforeStep = result->getCurrentVirtualTime();
    updateFromImage(input);
    step(dt);
    applyStepToImage(input, result);
}

void Model::initFromImage(boost::shared_ptr<const orpheus::metamodel::MentalImage> img)
{
    _virtualTimeBeforeStep = img->getCurrentVirtualTime();
    updateFromImage(img);
    //step(0.01);
}

uint32_t Model::_getVirtualTimeBeforeStep()
{
    return _virtualTimeBeforeStep;
}

void Model::setPermissions(ModelPermissions mp)
{
    _permissions = mp;
}

ModelPermissions& Model::accessPermissions()
{
    return _permissions;
}

//std::map<std::string, boost::any>& Model::accessParameters()
orpheus::value_specifications::ValueBag& Model::accessParameters()
{
    return _parameters;
}

const orpheus::value_specifications::ValueBag& Model::getParameters() const
{
    return _parameters;
}

bool Model::hasParameter(std::string param)
{
    return (_parameters.content().find(param) != _parameters.content().end());
}

}
}
