//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#include "Models/modelPermissions.h"

#include "Metamodel/mentalImage.h"

namespace orpheus
{
namespace models
{

ModelPermissions::ModelPermissions()
{
	
}

ModelPermissions::~ModelPermissions()
{
	
}

void ModelPermissions::grantAll(boost::shared_ptr<orpheus::metamodel::MentalImage> img)
{
    for (size_t i=0; i<img->size(); ++i)
    {
        _allowedObjects[img->getObject(i).id] = true;
    }
}

void ModelPermissions::grantPermission(boost::uuids::uuid id)
{
    _allowedObjects[id] = true;
}

void ModelPermissions::denyPermission(boost::uuids::uuid id)
{
    std::map<boost::uuids::uuid, bool>::iterator it=_allowedObjects.find(id);
    if (it != _allowedObjects.end())
    {
        _allowedObjects.erase(it);
    }
}

void ModelPermissions::clearAll()
{
    _allowedObjects.clear();
}

bool ModelPermissions::hasPermission(boost::uuids::uuid id)
{
    std::map<boost::uuids::uuid, bool>::iterator it=_allowedObjects.find(id);
    if (it != _allowedObjects.end())
    {
        return true;
    }
    return false;
}

std::vector<boost::uuids::uuid> ModelPermissions::getPermissionList()
{
    std::vector<boost::uuids::uuid> ret;
    for(std::map<boost::uuids::uuid, bool>::iterator it = _allowedObjects.begin(); it != _allowedObjects.end(); ++it)
    {
        ret.push_back(it->first);
    }
    return ret;
}

}
}
