//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#include "ValueSpecifications/valueBag.h"

#include "Tools/base64.h"
#include <boost/tokenizer.hpp>

#include "ValueSpecifications/valueSpecification.h"

namespace orpheus
{
namespace value_specifications
{

ValueBag::ValueBag()
{
    
}

ValueBag::~ValueBag()
{
    
}

//serialization purpose
std::string ValueBag::toString()
{
    std::string str = "";
    
    std::map<std::string, boost::shared_ptr<ValueSpecification> >::iterator it;
    for (it = _contents.begin(); it != _contents.end(); ++it)
    {
        if (it != _contents.begin())
        {
            str += ":";
        }
        std::string vs = base64_encode(it->first)+":"+((it->second)->toString());
        str += base64_encode(vs);
    }
    
    return base64_encode(str);
}

ValueBag ValueBag::fromString(std::string str)
{
    ValueBag bag;
    
    const std::string &decodedString = base64_decode(str);
    boost::char_separator<char> sep(":");
    boost::tokenizer<boost::char_separator<char> > tokens(decodedString, sep);
    for (boost::tokenizer<boost::char_separator<char> >::iterator tok_iter = tokens.begin(); tok_iter!=tokens.end(); ++tok_iter)
    {
        const std::string token = (*tok_iter);
        const std::string &decodedToken = base64_decode(token);
        
        std::size_t found = decodedToken.find(":");
        
        if (found != std::string::npos)
        {
            bag._contents[base64_decode(decodedToken.substr(0, found))] = ValueSpecification::fromString(decodedToken.substr(found+1));
        }
    }
    
    return bag;
}

std::map<std::string, boost::shared_ptr<ValueSpecification> >& ValueBag::content()
{
    return _contents;
}

bool ValueBag::contains(std::string key) const
{
    return (_contents.find(key) != _contents.end());
}

boost::shared_ptr<const ValueSpecification> ValueBag::get(std::string key) const
{
    if (contains(key)) return _contents.at(key);
    
    return boost::shared_ptr<ValueSpecification>();
}

ValueBag ValueBag::copy() const
{
    ValueBag bag;
    
    std::map<std::string, boost::shared_ptr<ValueSpecification> >::const_iterator it;
    for (it = _contents.begin(); it != _contents.end(); ++it)
    {
        bag._contents[it->first] = (it->second)->copy();
    }
    
    return bag;
}

void ValueBag::setValuesFrom(const ValueBag &bag)
{
    std::map<std::string, boost::shared_ptr<ValueSpecification> >::const_iterator it;
    for (it = bag._contents.begin(); it != bag._contents.end(); ++it)
    {
        _contents[it->first] = (it->second)->copy();
    }
}

}
}
