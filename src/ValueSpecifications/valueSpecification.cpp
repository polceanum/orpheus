//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#include "ValueSpecifications/valueSpecification.h"

#include <iostream>

#include <boost/lexical_cast.hpp>
#include "Tools/base64.h"

#include "ValueSpecifications/boolSpecification.h"
#include "ValueSpecifications/intSpecification.h"
#include "ValueSpecifications/longLongSpecification.h"
#include "ValueSpecifications/floatSpecification.h"
#include "ValueSpecifications/doubleSpecification.h"
#include "ValueSpecifications/stringSpecification.h"

namespace orpheus
{
namespace value_specifications
{

ValueSpecification::ValueSpecification()
{
    
}

ValueSpecification::~ValueSpecification()
{
    
}

bool ValueSpecification::asBool() const
{
    return boost::lexical_cast<bool>(asString());
}

int ValueSpecification::asInt() const
{
    return boost::lexical_cast<int>(asString());
}

long long ValueSpecification::asLongLong() const
{
    return boost::lexical_cast<long long>(asString());
}

float ValueSpecification::asFloat() const
{
    return boost::lexical_cast<float>(asString());
}

double ValueSpecification::asDouble() const
{
    return boost::lexical_cast<double>(asString());
}

boost::shared_ptr<ValueSpecification> ValueSpecification::Bool(bool value)
{
    return boost::shared_ptr<BoolSpecification>(new BoolSpecification(value));
}

boost::shared_ptr<ValueSpecification> ValueSpecification::Int(int value)
{
    return boost::shared_ptr<IntSpecification>(new IntSpecification(value));
}

boost::shared_ptr<ValueSpecification> ValueSpecification::LongLong(long long value)
{
    return boost::shared_ptr<LongLongSpecification>(new LongLongSpecification(value));
}

boost::shared_ptr<ValueSpecification> ValueSpecification::Float(float value)
{
    return boost::shared_ptr<FloatSpecification>(new FloatSpecification(value));
}

boost::shared_ptr<ValueSpecification> ValueSpecification::Double(double value)
{
    return boost::shared_ptr<DoubleSpecification>(new DoubleSpecification(value));
}

boost::shared_ptr<ValueSpecification> ValueSpecification::String(std::string value)
{
    return boost::shared_ptr<StringSpecification>(new StringSpecification(value));
}

//serialization purpose
std::string ValueSpecification::toString() const
{
    std::string str = "";
    str += _type+":";
    str += base64_encode(asString());
    return base64_encode(str);
}

boost::shared_ptr<ValueSpecification> ValueSpecification::fromString(const std::string &str)
{
    boost::shared_ptr<ValueSpecification> val;
    const std::string &decoded = base64_decode(str);
    
    size_t found = decoded.find(":");
    std::string type;
    std::string value;
    
    if (found != std::string::npos)
    {
        type = decoded.substr(0, found);
        value = decoded.substr(found+1);
        value = base64_decode(value);
    }
    else
    {
        std::cerr << "ValueSpecification deserialization format error !" << std::endl;
        return val;
    }
    
    if (type == "TYPE_BOOL")
    {
        val = Bool(boost::lexical_cast<bool>(value));
    }
    else if (type == "TYPE_INT")
    {
        val = Int(boost::lexical_cast<int>(value));
    }
    else if (type == "TYPE_LONGLONG")
    {
        val = LongLong(boost::lexical_cast<long long>(value));
    }
    else if (type == "TYPE_FLOAT")
    {
        val = Float(boost::lexical_cast<float>(value));
    }
    else if (type == "TYPE_DOUBLE")
    {
        val = Double(boost::lexical_cast<double>(value));
    }
    else if (type == "TYPE_STRING")
    {
        val = String(boost::lexical_cast<std::string>(value));
    }
    else
    {
        std::cerr << "ValueSpecification deserialization type error !" << std::endl;
    }
    
    return val;
}

}
}
