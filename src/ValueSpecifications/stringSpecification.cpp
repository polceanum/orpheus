//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#include "ValueSpecifications/stringSpecification.h"

#include <boost/lexical_cast.hpp>

namespace orpheus
{
namespace value_specifications
{

StringSpecification::StringSpecification(std::string val) : ValueSpecification(), _value(val)
{
    _type = "TYPE_STRING";
}

StringSpecification::~StringSpecification()
{
    
}

std::string StringSpecification::asString() const
{
    return _value;
}

boost::shared_ptr<ValueSpecification> StringSpecification::copy() const
{
    boost::shared_ptr<ValueSpecification> c = boost::shared_ptr<StringSpecification>(new StringSpecification(_value));
    return c;
}

}
}
