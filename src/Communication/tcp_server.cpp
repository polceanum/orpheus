//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#include "Communication/tcp_server.h"

#include "Communication/request.h"
#include "Communication/communicationManager.h"

//for requests:
#include "SimulationEngine/simulationEngine.h"
#include "SimulationEngine/simulationLocalStrange.h"
#include "SimulationEngine/simulationRemote.h"

#include <iostream>

namespace orpheus
{
namespace communication
{

tcp_server::tcp_server(boost::asio::io_service& io_service, std::string port) : _acceptor(io_service)
{
	int cPort = boost::lexical_cast<int>(port);
	int originalPort = cPort;
	
	bool success = false;
	
	while (!success)
	{
		try
		{
			if (_acceptor.is_open())
			{
				_acceptor.close();
			}
			boost::asio::ip::tcp::endpoint ep(boost::asio::ip::tcp::v4(), cPort);
			_acceptor.open(ep.protocol());
			_acceptor.bind(ep);
			_acceptor.listen();
		
			start_accept();
		
			_thread = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&boost::asio::io_service::run, &io_service)));
		
			std::cerr << "------------------------------------------------" << std::endl;
			std::cerr << "ORPHEUS communication server online on port " << cPort << std::endl;
			std::cerr << "------------------------------------------------" << std::endl;
			
			_port = boost::lexical_cast<std::string>(cPort);
			
			success = true;
		}
		catch (std::exception& e)
		{
			std::cerr << "TCP Server :: Thread exception on port " << cPort << " :: " << e.what() << std::endl;
		}
		
		cPort++;
		
		if (cPort-originalPort > 9)
		{
			std::cerr << "TCP Server :: 9 Ports exhausted :: giving up..." << std::endl;
			break;
		}
	}
}

tcp_server::~tcp_server()
{

}

std::string tcp_server::getPort()
{
	return _port;
}

void tcp_server::setManager(boost::weak_ptr<CommunicationManager> mgr)
{
	_manager = mgr;
}

void tcp_server::handle_request(boost::shared_ptr<request> req)
{
#ifndef _ORPHEUS_GUI_
	//std::cerr << "Handling request:" << std::endl;
    
    std::size_t found = req->msg.find(".");
    std::string msgCmd = req->msg;
    std::string msgParam = "";
    
    if (found != std::string::npos)
    {
        msgCmd = req->msg.substr(0, found);
        msgParam = req->msg.substr(found+1);
    }
	
	if (msgCmd == "SimulationStateSubscribe")
    {
        int simIndex = boost::lexical_cast<int>(msgParam);
        if (simIndex < 0) simIndex = 0;
        
        std::cerr << "-> subscribing " << req->ip << ":" << req->port << " to simulation " << simIndex << " stream" << std::endl;
        
        boost::shared_ptr<CommunicationManager> mgr = _manager.lock();
        if (mgr && (orpheus::simulation_engine::SimulationEngine::getInstance()->size() > simIndex))
        {
            orpheus::simulation_engine::SimulationEngine::getInstance()->accessSimulation(simIndex)->addSubscriber(mgr->getClient(req->ip, req->port));
            orpheus::simulation_engine::SimulationEngine::getInstance()->addSubscriber(simIndex, mgr->getClient(req->ip, req->port)); //just add to list
        }
        else if (mgr)
        {
            orpheus::simulation_engine::SimulationEngine::getInstance()->addSubscriber(simIndex, mgr->getClient(req->ip, req->port));
            std::cerr << "-> no such simulation #" << simIndex << " :: adding subscriber to list and will assign simulation as soon as available" << std::endl;
        }
        else
        {
            std::cerr << "-> subscription ignored, problem with the manager" << simIndex << std::endl; //should not happen
        }
    }
    else if (msgCmd == "SimulationStateUnsubscribe")
    {
        std::cerr << "-> unsubscribing " << req->ip << ":" << req->port << " from simulation stream" << std::endl;
        
        //... not yet implemented because unsubscription is automatically made when connection with client is lost
    }
    else if (msgCmd == "BindStrangeSimulation") //client requests the creation of a local strange simulation here, which it will control remotely
    {
        std::size_t paramFound0 = msgParam.find(".");
        //std::size_t paramFound1 = msgParam.find(".", paramFound0+1);
        //std::size_t paramFound2 = msgParam.find(".", paramFound1+1);
        
        boost::uuids::string_generator gen;
        boost::uuids::uuid simId(gen(msgParam.substr(0, paramFound0)));
        std::string imgString = msgParam.substr(paramFound0+1);
        
        //std::cerr << "-> creating local strange simulation for " << req->ip << ":" << req->port << " with id: " << msgParam.substr(0, paramFound0) << std::endl;
        
        boost::shared_ptr<CommunicationManager> mgr = _manager.lock();
        if (mgr)
        {
            boost::shared_ptr<orpheus::metamodel::MentalImage> img = orpheus::metamodel::MentalImage::fromString(imgString);
            boost::shared_ptr<orpheus::simulation_engine::SimulationLocalStrange> sim = boost::shared_ptr<orpheus::simulation_engine::SimulationLocalStrange>(new orpheus::simulation_engine::SimulationLocalStrange(simId, img, mgr->getClient(req->ip, req->port)));
            
            orpheus::simulation_engine::SimulationEngine::getInstance()->addSimulation(sim);
            
            //... test
            double rnd = ((double)rand()/RAND_MAX);
            sim->setMaxRunTime(5.0+5.0*rnd);
            sim->setStepSize(/*_unifiedStepSize*/0.10);
            sim->start();
        }
    }
    else if (msgCmd == "UpdateRemoteSimulation")
    {
        std::size_t paramFound0 = msgParam.find(".");
        std::size_t paramFound1 = msgParam.find(".", paramFound0+1);
        //std::size_t paramFound2 = msgParam.find(".", paramFound1+1);
        
        boost::uuids::string_generator gen;
        boost::uuids::uuid simId(gen(msgParam.substr(0, paramFound0)));
        bool goalAchieved = boost::lexical_cast<bool>(msgParam.substr(paramFound0+1, paramFound1-(paramFound0+1)));
        std::string simParams = msgParam.substr(paramFound1+1);
        
        boost::shared_ptr<orpheus::simulation_engine::SimulationRemote> rs = boost::dynamic_pointer_cast<orpheus::simulation_engine::SimulationRemote>(orpheus::simulation_engine::SimulationEngine::getInstance()->accessSimulation(simId));
        rs->deserializeSimulationControllerHistory(simParams);
        rs->setGoalAchieved(goalAchieved);
        rs->setFinished();
        
        //std::cerr << "-> remote simulation on " << req->ip << ":" << req->port << " has finished ... id: " << msgParam.substr(0, paramFound0) << std::endl;
    }
	else
	{
		std::cerr << "-> Unknown request." << std::endl;
	}
#endif //_ORPHEUS-GUI_
}

void tcp_server::postMessage(std::string message)
{
	std::size_t found;
	if (found=message.find("Request:")!=std::string::npos) // Request:something:Return:127.0.0.1:8090
	{
		std::string requestBody = message.substr(found+7); // strip "Request:". get the rest
		//std::cerr << "got Request: " << requestBody << std::endl;
		boost::shared_ptr<request> req = boost::shared_ptr<request>(new request(requestBody));
		
		handle_request(req); //yes. too complicated to make a separate thread which can treat requests for now.
	}
	else
	{
		std::size_t found;
		if ((found=message.find(":"))!=std::string::npos) //strip ip tag
		{
			//std::cerr << "found = " << found << std::endl;
			//std::cerr << "got this message: " << message.substr(0,found) << std::endl;
			_messagePostMutex.lock();
				_mailBox.push_back(message.substr(0,found));
			_messagePostMutex.unlock();
		}
		else
		{
			std::cerr << "Invalid message received (no ip tag)" << std::endl;
		}
	}
}

std::string tcp_server::getLastMessage()
{
	std::string lastMsg = "";
	_messagePostMutex.lock();
		if (_mailBox.size()>0)
		{
			lastMsg = _mailBox[0];
			_mailBox.erase(_mailBox.begin());
		}
	_messagePostMutex.unlock();
	
	return lastMsg;
}

//----------------------------------------------------------------------------//

void tcp_server::disconnectAllClients()
{
    for (size_t i=0; i<_connections.size(); ++i)
    {
        boost::shared_ptr<tcp_connection> conn = _connections[i].lock();
        if (conn)
        {
            conn->close();
        }
    }
    _connections.clear();
}

void tcp_server::start_accept()
{
    boost::shared_ptr<tcp_connection> new_connection = tcp_connection::create(_acceptor.get_io_service());

	_acceptor.async_accept(new_connection->socket(), boost::bind(&tcp_server::handle_accept, this, new_connection, boost::asio::placeholders::error));
}

void tcp_server::handle_accept(boost::shared_ptr<tcp_connection> new_connection, const boost::system::error_code& error)
{
	if (!error)
	{
		new_connection->start();
		new_connection->setMessagePost(shared_from_this());
		_connections.push_back(new_connection);
		start_accept();
	}
}

}
}
