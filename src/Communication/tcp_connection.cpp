//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#include "Communication/tcp_connection.h"

#include "Communication/tcp_server.h"

#include <iostream>

namespace orpheus
{
namespace communication
{

boost::shared_ptr<tcp_connection> tcp_connection::create(boost::asio::io_service& io_service)
{
	return boost::shared_ptr<tcp_connection>(new tcp_connection(io_service));
}

boost::asio::ip::tcp::socket& tcp_connection::socket()
{
	return _socket;
}

void tcp_connection::start()
{
	_receivedBulk.str("");
	
	start_read();
}

void tcp_connection::start_write(std::string msg)
{
	boost::system::error_code error;
	
	_socket.write_some(boost::asio::buffer(msg), error);
    
    if (error)
    {
        _socket.close();
    }
    
    //boost::asio::write(_socket, boost::asio::buffer(msg), boost::asio::transfer_all());
}

void tcp_connection::start_async_write(std::string msg)
{
	_socket.async_write_some(boost::asio::buffer(msg), boost::bind(&tcp_connection::handle_write, shared_from_this(), boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}

void tcp_connection::start_read()
{
	_socket.async_read_some(boost::asio::buffer(_readBuffer, (BUFFER_SIZE-1)*sizeof(char)), boost::bind(&tcp_connection::handle_read, shared_from_this(), boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}

bool tcp_connection::is_open()
{
    return _socket.is_open();
}

void tcp_connection::close()
{
    boost::system::error_code error;
    _socket.shutdown(boost::asio::ip::tcp::socket::shutdown_both, error);
	_socket.close();
}

void tcp_connection::establish(std::string host, std::string port)
{
    boost::asio::ip::tcp::resolver resolver(_socket.get_io_service());
	boost::asio::ip::tcp::resolver::query query(host.c_str(), port.c_str());
	boost::asio::ip::tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
	boost::asio::ip::tcp::resolver::iterator end;
	
	boost::system::error_code error = boost::asio::error::host_not_found;
	while (error && endpoint_iterator != end)
	{
		_socket.close();
		_socket.connect(*endpoint_iterator++, error);
	}
	if (error)
		throw boost::system::system_error(error);
}

std::string tcp_connection::getLastReceivedMessage() //must be thread safe
{
	std::string response("");
	
	if (_mutex.try_lock())
	{
		response = _lastReceivedMessage;
		_mutex.unlock();
	}
	
	return response;
}

//-------------------------------------------------------------------------------------------------------------------/

tcp_connection::tcp_connection(boost::asio::io_service& io_service) : _socket(io_service)
{
	//constructor
    //boost::asio::socket_base::keep_alive option(true);
    //_socket.set_option(option);
}

tcp_connection::~tcp_connection()
{
	//destructor
}

void tcp_connection::handle_read(const boost::system::error_code& error, size_t bytes_transferred)
{
	if (!error)
	{
		size_t nrOfChars = (bytes_transferred/sizeof(char));
		
		for (size_t i=0; i<nrOfChars; ++i)
		{
			if (_readBuffer.data()[i] == '\n')
			{
				boost::shared_ptr<tcp_server> serv = _messagePost.lock();
				if (serv)
				{
					serv->postMessage(_receivedBulk.str() + std::string(":") + _socket.remote_endpoint().address().to_string()); //append remote address to message
				}
				else
				{
					std::cerr << "Message post server is null." << std::endl;
				}
				
				_receivedBulk.str(std::string());
			}
			else
			{
				_receivedBulk.put(_readBuffer.data()[i]);
			}
		}
		
		start_read();
	}
}

void tcp_connection::handle_write(const boost::system::error_code& error, size_t bytes_transferred)
{
	//nothing to do
}

void tcp_connection::setMessagePost(boost::weak_ptr<tcp_server> server)
{
	_messagePost = server;
}

}
}
