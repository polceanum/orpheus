//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#include "Communication/request.h"

#include <iostream>

namespace orpheus
{
namespace communication
{

request::request(std::string whole) //whole string = msg:port:ip
{
	std::size_t found1 = whole.find(":");
	msg = whole.substr(0, found1);
	std::size_t found2 = whole.find(":", found1+1);
	port = whole.substr(found1+1,found2-found1-1);
	ip = whole.substr(found2+1);
	
	//std::cerr << whole << " => " << msg << ", <" << ip << "> : <" << port << ">" << std::endl;
}

request::request(std::string message, std::string respondIP, std::string respondPort)
{
	msg = message;
	ip = respondIP;
	port = respondPort;
}

request::~request(void)
{

}

}
}
