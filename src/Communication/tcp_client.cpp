//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#include "Communication/tcp_client.h"

namespace orpheus
{
namespace communication
{

tcp_client::tcp_client(boost::asio::io_service& io_service)
{
	_connection = tcp_connection::create(io_service);
}

tcp_client::~tcp_client()
{

}

void tcp_client::connect(std::string host, std::string port)
{
    _host = host;
    _port = port;
	_connection->establish(host, port);
}

void tcp_client::send(std::string msg)
{
    _connection->start_write(msg);
}

void tcp_client::async_send(std::string msg)
{
	_connection->start_async_write(msg);
}

bool tcp_client::is_connected()
{
    return _connection->is_open();
}

void tcp_client::reconnect()
{
    connect(_host, _port);
}

void tcp_client::disconnect()
{
	_connection->close();
}

std::string tcp_client::getHost()
{
    return _host;
}

std::string tcp_client::getPort()
{
    return _port;
}

}
}
