//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#include "Communication/communicationManager.h"

namespace orpheus
{
namespace communication
{

std::map<std::string, boost::shared_ptr<CommunicationManager> > CommunicationManager::_instances;
boost::asio::io_service CommunicationManager::_io_service;

CommunicationManager::CommunicationManager()
{
	_server = boost::shared_ptr<tcp_server>(new tcp_server(_io_service, "8090"));
}

CommunicationManager::~CommunicationManager()
{
	
}

boost::shared_ptr<CommunicationManager> CommunicationManager::getInstance(std::string aid)
{
	std::map<std::string, boost::shared_ptr<CommunicationManager> >::iterator inst = _instances.find(aid);
	if (inst != _instances.end())
	{
		return inst->second;
	}
	
	boost::shared_ptr<CommunicationManager> cmgr = boost::shared_ptr<CommunicationManager>(new CommunicationManager());
	cmgr->getServer()->setManager(cmgr);
	_instances[aid] = cmgr;
	return cmgr;
}

boost::shared_ptr<tcp_server> CommunicationManager::getServer()
{
	return _server;
}

boost::shared_ptr<tcp_client> CommunicationManager::getClient(std::string host, std::string port, bool autoConnect)
{
	std::map<std::string, boost::shared_ptr<tcp_client> >::iterator c = _clients.find(host+":"+port);
	
	if (c != _clients.end())
	{
        if (!(c->second)->is_connected())
        {
            (c->second)->reconnect();
        }
		return c->second;
	}
	
    if (autoConnect)
    {
        boost::shared_ptr<tcp_client> client = boost::shared_ptr<tcp_client>(new tcp_client(_io_service));
        client->connect(host, port);
        _clients[host+":"+port] = client;
        
        return client;
    }
    
    return boost::shared_ptr<tcp_client>();
}

void CommunicationManager::destroyClient(boost::shared_ptr<tcp_client> client)
{
    std::map<std::string, boost::shared_ptr<tcp_client> >::iterator c = _clients.find(client->getHost()+":"+client->getPort());
    
    if (c != _clients.end())
	{
        if ((c->second)->is_connected())
        {
            (c->second)->disconnect();
        }
		_clients.erase(c);
	}
}

}
}
