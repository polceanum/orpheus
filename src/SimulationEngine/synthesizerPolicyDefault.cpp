//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#include "SimulationEngine/synthesizerPolicyDefault.h"

#include "ValueSpecifications/valueSpecification.h"

namespace orpheus
{
namespace simulation_engine
{

SynthesizerPolicyDefault::SynthesizerPolicyDefault() : SynthesizerPolicy()
{
    
}

SynthesizerPolicyDefault::~SynthesizerPolicyDefault()
{
    
}

bool SynthesizerPolicyDefault::apply(boost::shared_ptr<Simulation> source, std::string tag, std::vector<orpheus::value_specifications::ValueBag>& actions)
{
    //if (source->getGoal()->isSatisfied())
    {
        double satisfaction = source->getGoal()->getSatisfaction();
        
        std::vector<orpheus::value_specifications::ValueBag> history = source->getSimulationControllerHistory(tag);
        if (history.size() > 0)
        {
            long long startTime = history[0].content()["realtimestamp"]->asLongLong();
            
            /*
            //debugging
            std::cerr << "Before:" << std::endl;
            for (std::size_t i=0; i<actions.size(); ++i)
            {
                std::cerr << "(" << actions[i].content()["realtimestamp"]->asLongLong() << "," << actions[i].content()["satisfactionLevel"]->asDouble() << ") ";
            }
            std::cerr << std::endl;
            */
            
            //assuming compatibility (actions transfered)
            
            //search action sequence for the start time of the source
            std::size_t index=0;
            while ((index<actions.size()) && (actions[index].content()["realtimestamp"]->asLongLong() < startTime)) index++;
            
            if (index<actions.size())
            {
                if (actions[index].content()["satisfactionLevel"]->asDouble() < satisfaction)
                {
                    //remove from index to the end
                    actions.erase(actions.begin()+index, actions.end());
                }
                else
                {
                    return false;
                }
            }
            
            //add actions from source
            for (std::size_t i=0; i<history.size(); ++i)
            {
                orpheus::value_specifications::ValueBag copy = history[i].copy();
                copy.content()["satisfactionLevel"] = orpheus::value_specifications::ValueSpecification::Double(satisfaction);
                actions.push_back(copy);
            }
            
            /*
            //debugging
            std::cerr << "After:" << std::endl;
            for (std::size_t i=0; i<actions.size(); ++i)
            {
                std::cerr << "(" << actions[i].content()["realtimestamp"]->asLongLong() << "," << actions[i].content()["satisfactionLevel"]->asDouble() << ") ";
            }
            std::cerr << std::endl;
            */
            
            return true;
        }
    }
    
    return false;
}

}
}
