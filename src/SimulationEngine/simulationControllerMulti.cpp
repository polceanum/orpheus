//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#include "SimulationEngine/simulationControllerMulti.h"

#include "ValueSpecifications/valueSpecification.h"

namespace orpheus
{
namespace simulation_engine
{

SimulationControllerMulti::SimulationControllerMulti(std::string tag, std::vector<boost::shared_ptr<orpheus::models::Model> > targets) : SimulationController(tag), _targetModels(targets)
{
    
}

SimulationControllerMulti::~SimulationControllerMulti()
{
    
}

void SimulationControllerMulti::mutateModelParameters(boost::shared_ptr<const orpheus::metamodel::MentalImage> img, bool actionsAllowed)
{
    //Note that this is not perfect. It is possible to overwrite some parameter values due to the many model design. Use with caution !
    
    if (actionsAllowed)
    {
        if (mutateParameterHook(img))
        {
            orpheus::value_specifications::ValueBag params;
            for (std::size_t i=0; i<_targetModels.size(); ++i)
            {
                params.setValuesFrom(_targetModels[i]->accessParameters());
            }
            params.content()["realtimestamp"] = orpheus::value_specifications::ValueSpecification::LongLong(img->getCurrentVirtualTime());
            _parameterHistory.push_back(params);
        }
    }
    else
    {
        while (_transferredParameterHistory.size() > 0 && _transferredParameterHistory[0].content()["realtimestamp"]->asLongLong() <= img->getCurrentVirtualTime())
        {
            for (std::size_t i=0; i<_targetModels.size(); ++i)
            {
                orpheus::value_specifications::ValueBag& params = _targetModels[i]->accessParameters();
                params.setValuesFrom(_transferredParameterHistory[0]);
            }
            _transferredParameterHistory.erase(_transferredParameterHistory.begin());
        }
    }
}

}
}
