//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#include "SimulationEngine/simulationControllerSingle.h"

#include "ValueSpecifications/valueSpecification.h"

namespace orpheus
{
namespace simulation_engine
{

SimulationControllerSingle::SimulationControllerSingle(std::string tag, boost::shared_ptr<orpheus::models::Model> target) : SimulationController(tag), _targetModel(target)
{
    
}

SimulationControllerSingle::~SimulationControllerSingle()
{
    
}

void SimulationControllerSingle::mutateModelParameters(boost::shared_ptr<const orpheus::metamodel::MentalImage> img, bool actionsAllowed)
{
    if (actionsAllowed)
    {
        if (mutateParameterHook(img))
        {
            orpheus::value_specifications::ValueBag params = _targetModel->accessParameters().copy();
            params.content()["realtimestamp"] = orpheus::value_specifications::ValueSpecification::LongLong(img->getCurrentVirtualTime());
            
            _parameterHistory.push_back(params);
        }
    }
    else
    {
        while (_transferredParameterHistory.size() > 0 && _transferredParameterHistory[0].content()["realtimestamp"]->asLongLong() <= img->getCurrentVirtualTime())
        {
            orpheus::value_specifications::ValueBag& params = _targetModel->accessParameters();
            params.setValuesFrom(_transferredParameterHistory[0]);
            _transferredParameterHistory.erase(_transferredParameterHistory.begin());
        }
    }
}

}
}
