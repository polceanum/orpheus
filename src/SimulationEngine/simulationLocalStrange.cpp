//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#include "SimulationEngine/simulationLocalStrange.h"

#include "SimulationEngine/simulationEngine.h"

#include "Communication/communicationManager.h"

#include "Tools/base64.h"

#include <boost/uuid/uuid_io.hpp>

#include <iomanip>      // std::setprecision


#include "ModelAssignment/modelAssignment.h"

namespace orpheus
{
namespace simulation_engine
{

SimulationLocalStrange::SimulationLocalStrange(boost::uuids::uuid id, boost::shared_ptr<orpheus::metamodel::MentalImage> img, boost::shared_ptr<orpheus::communication::tcp_client> master)
    : SimulationLocal(img), _masterClient(master), _requestTransferTime(0ll)
{
	_simulationId = id;
}

SimulationLocalStrange::~SimulationLocalStrange()
{
    destroy();
    //std::cerr << "~SimulationLocalStrange()" << std::endl;
}

void SimulationLocalStrange::start()
{
    //----------------//
    //REMOVED due to change to independent image time ! therefore, to calculate the requestTransferTime, the message should be timestamped, and used in the calculation
    /*
    boost::posix_time::ptime imgTime = _simulationImage->getZeroTime();
    boost::posix_time::ptime now = boost::posix_time::microsec_clock::local_time();
    
    boost::posix_time::ptime epoch(boost::gregorian::date(1970,1,1));
    long long imgMillis = (imgTime - epoch).total_milliseconds();
    long long nowMillis = (now - epoch).total_milliseconds();
    _requestTransferTime = nowMillis - imgMillis;
    */
    //----------------//
    
    /*
    //devel stuff
    std::cerr << "Received remote simulation with timestamp: " << imgMillis << " at time: " << nowMillis << " -- diff = " << (nowMillis-imgMillis)/1000.0 << std::endl;
    */
    
    //out of convenience for now
    setSubscriberFeedInterval(120);
    
    orpheus::model_assignment::ModelAssignment::getInstance()->assignModelsToSimulation(shared_from_this());
    
    //add controller
    //boost::shared_ptr<orpheus::simulation_engine::SimulationController> ctrl = boost::shared_ptr<orpheus::simulation_engine::SimulationController>(new orpheus::simulation_engine::SimulationController());
    //setSimulationController(ctrl);
    
    //add goal
    //boost::shared_ptr<orpheus::goal_system::Goal> goal = boost::shared_ptr<orpheus::goal_system::Goal>(new orpheus::goal_system::Goal());
    //setGoal(goal);
    
    SimulationLocal::start();
}

void SimulationLocalStrange::worker()
{
	SimulationLocal::worker();
    
    //inform master client that the simulation is over
    std::string requestString = "Request:UpdateRemoteSimulation";
    requestString += "."+boost::lexical_cast<std::string>(_simulationId);
    
    if (_goal)
    {
        requestString += "."+boost::lexical_cast<std::string>(_goal->isSatisfied());
    }
    else
    {
        requestString += "."+boost::lexical_cast<std::string>(false);
    }
    
    //TODO TODO: fix this
    //std::vector<orpheus::value_specifications::ValueBag> history = getSimulationControllerHistory();
    std::string historyStr = "";
    /*
    for (std::size_t i=0; i<history.size(); ++i)
    {
        if (i>0) historyStr += ":";
        historyStr += history[i].toString();
    }
    */
    requestString += "."+base64_encode(historyStr);
    
    //devel stuff
    /*
    boost::posix_time::ptime now = boost::posix_time::microsec_clock::local_time();
    boost::posix_time::ptime epoch(boost::gregorian::date(1970,1,1));
    long long nowMillis = (now - epoch).total_milliseconds();
    std::cerr << "Sending back simulation result at time: " << nowMillis << " -- " << getId() << std::endl;
    */
    
    _masterClient->send(requestString+":"+orpheus::communication::CommunicationManager::getInstance("me")->getServer()->getPort()+"\n");
    
    orpheus::simulation_engine::SimulationEngine::getInstance()->removeSimulation(_simulationId);
}

double SimulationLocalStrange::_getTimeBeforeActions()
{
    double timeBeforeActions = SimulationLocal::_getTimeBeforeActions();
    
    timeBeforeActions += (_requestTransferTime/1000.0);
    
    timeBeforeActions += 0.02; //acount for result communication ... TODO: make this smarter
    
    return timeBeforeActions;
}

}
}
