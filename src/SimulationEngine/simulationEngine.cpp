//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#include "SimulationEngine/simulationEngine.h"

#include <boost/uuid/uuid_io.hpp>

namespace orpheus
{
namespace simulation_engine
{

boost::shared_ptr<SimulationEngine> SimulationEngine::_instance;

SimulationEngine::SimulationEngine() //private
{
    
}

SimulationEngine::~SimulationEngine()
{
    
}

boost::shared_ptr<SimulationEngine> SimulationEngine::getInstance()
{
	if (!_instance)
	{
		_instance = boost::shared_ptr<SimulationEngine>(new SimulationEngine());
        
        //start garbage collector
        _instance->_garbageMan = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&SimulationEngine::_collectGarbage, _instance)));
	}
	
	return _instance;
}

void SimulationEngine::addSimulation(boost::shared_ptr<Simulation> sim)
{
	for (int i=0; i<_simulations.size(); ++i)
	{
		if (_simulations[i] == sim)
		{
			std::cerr << "Trying to add already existing simulation, skipping." << std::endl;
			return;
		}
	}
	
    _simMutex.lock();
	_simulations.push_back(sim);
    _simMutex.unlock();
    
    //add subscribers
    if (_subscriberLists.find(_simulations.size()-1) != _subscriberLists.end())
    {
        std::vector<boost::shared_ptr<orpheus::communication::tcp_client> > &subs = _subscriberLists.at(_simulations.size()-1);
        for (std::size_t i=0; i<subs.size(); ++i)
        {
            if (!subs[i]->is_connected())
            {
                std::cerr << "Lost connection to " << subs[i]->getHost() << ":" << subs[i]->getPort() << " ... removing from subscribers list." << std::endl;
                subs.erase(subs.begin()+i);
                i--;
                continue;
            }
            
            if (!sim->hasSubscriber(subs[i]))
            {
                sim->addSubscriber(subs[i]);
            }
        }
    }
}

//hidious memory leak happens sometimes due to the garbage collection strategy. TODO: FIX IT !!
void SimulationEngine::removeSimulation(std::size_t index)
{
    if (_simulations.size()==0) std::cerr << "Requested simulation removal, but no simulations exist. Crashing..." << std::endl;
    _garbageMutex.lock();
        _garbage.push_back(_simulations[index]);
    _garbageMutex.unlock();
    
	_simulations.erase(_simulations.begin()+index);
}

void SimulationEngine::removeSimulation(boost::shared_ptr<Simulation> sim)
{
    if (!sim) return;
    
    _simMutex.lock();
    for (std::size_t i=0; i<_simulations.size(); ++i)
    {
        if (_simulations[i] == sim)
        {
            _simulations.erase(_simulations.begin()+i);
            break;
        }
    }
    _simMutex.unlock();
    
    _garbageMutex.lock();
        _garbage.push_back(sim);
    _garbageMutex.unlock();
}

void SimulationEngine::removeSimulation(boost::uuids::uuid id)
{
    boost::shared_ptr<Simulation> simToRemove;
    _simMutex.lock();
    for (int i=0; i<_simulations.size(); ++i)
    {
        if (_simulations[i]->getId() == id)
        {
            simToRemove = _simulations[i];
            _simulations.erase(_simulations.begin()+i);
            break;
        }
    }
    _simMutex.unlock();
    
    if (simToRemove)
    {
        _garbageMutex.lock();
            _garbage.push_back(simToRemove);
        _garbageMutex.unlock();
    }
}

//synchronous deletion (bypass garbage collector) to avoid escalation of memory leaks
void SimulationEngine::removeSimulationNow(boost::uuids::uuid id)
{
    boost::shared_ptr<Simulation> simToRemove;
    _simMutex.lock();
    for (int i=0; i<_simulations.size(); ++i)
    {
        if (_simulations[i]->getId() == id)
        {
            simToRemove = _simulations[i];
            _simulations.erase(_simulations.begin()+i);
            break;
        }
    }
    _simMutex.unlock();
    
    simToRemove->destroy();
}

boost::shared_ptr<Simulation> SimulationEngine::accessSimulation(std::size_t index)
{
    return _simulations[index];
}

boost::shared_ptr<Simulation> SimulationEngine::accessSimulation(boost::uuids::uuid id)
{
    for (int i=0; i<_simulations.size(); ++i)
    {
        if (_simulations[i]->getId() == id)
        {
            return _simulations[i];
        }
    }
    std::cerr << "Simulation " << id << " not found !" << std::endl;
    return boost::shared_ptr<Simulation>();
}

std::size_t SimulationEngine::size()
{
	return _simulations.size();
}

void SimulationEngine::addSubscriber(std::size_t simIndex, boost::shared_ptr<orpheus::communication::tcp_client> client)
{
    if (_subscriberLists.find(simIndex) == _subscriberLists.end())
    {
        _subscriberLists.insert ( std::pair<std::size_t, std::vector<boost::shared_ptr<orpheus::communication::tcp_client> > >(simIndex, std::vector<boost::shared_ptr<orpheus::communication::tcp_client> >()) );
    }
    
    std::vector<boost::shared_ptr<orpheus::communication::tcp_client> > &subs = _subscriberLists.at(simIndex);
    for (std::size_t i=0; i<subs.size(); ++i)
    {
        if (subs[i] == client)
        {
            std::cerr << "SimulationEngine::addSubscriber() : Subscriber already in list. Ignoring request." << std::endl;
            return;
        }
    }
    
    subs.push_back(client);
}

void SimulationEngine::_collectGarbage()
{
    while (true)
    {
        if (_garbageMutex.try_lock())
        {
            if (_garbage.size() > 0)
            {
                _garbage[0]->destroy();
                _garbage.erase(_garbage.begin());
            }
            _garbageMutex.unlock();
        }
        //boost::this_thread::sleep(boost::posix_time::milliseconds(10));
        boost::this_thread::sleep_for(boost::chrono::milliseconds(5));
    }
}

}
}
