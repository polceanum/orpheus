//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#include "SimulationEngine/actionSynthesizer.h"

#include "SimulationEngine/simulationEngine.h"
#include "ValueSpecifications/valueSpecification.h"

#include <boost/uuid/uuid_io.hpp>

#include <boost/chrono.hpp>

namespace orpheus
{
namespace simulation_engine
{

ActionSynthesizer::ActionSynthesizer(std::string tag, boost::shared_ptr<SynthesizerPolicy> policy) : _working(false), _tag(tag), _policy(policy), _clearSimulations(false), _actionsSubmittedFromMainThread(false)
{
    
}

ActionSynthesizer::~ActionSynthesizer()
{
    
}

void ActionSynthesizer::setActionSequence(std::vector<orpheus::value_specifications::ValueBag> actions, bool abortOthers)
{
    _mutex.lock();
        if (abortOthers) _clearSimulations = true;
        _actions.clear();
        for (std::size_t i=0; i<actions.size(); ++i)
        {
            orpheus::value_specifications::ValueBag copy = actions[i].copy();
            _actions.push_back(copy);
        }
        if (abortOthers) _actionsSubmittedFromMainThread = true;
    _mutex.unlock();
}

void ActionSynthesizer::manageSimulation(boost::shared_ptr<Simulation> sim)
{
    _mutex.lock();
        
        std::vector<boost::shared_ptr<SimulationController> > controllers = sim->getSimulationControllers();
        for (std::size_t i=0; i<controllers.size(); ++i)
        {
            controllers[i]->transferParameterHistory(_actions, sim->getZeroTime());
        }
        
        SimulationEngine::getInstance()->addSimulation(sim);
        sim->start();
        
        _simulations.push_back(sim);
    _mutex.unlock();
}

std::size_t ActionSynthesizer::getNumberOfActionsToPerform()
{
    std::size_t nr;
    _mutex.lock();
        nr = _actions.size();
    _mutex.unlock();
    return nr;
}

std::vector<orpheus::value_specifications::ValueBag> ActionSynthesizer::getActionsToPerform()
{
    std::vector<orpheus::value_specifications::ValueBag> acts;
    _mutex.lock();
        for (std::size_t i=0; i<_actions.size(); ++i)
        {
            orpheus::value_specifications::ValueBag copy = _actions[i].copy();
            acts.push_back(copy);
        }
    _mutex.unlock();
    return acts;
}

void ActionSynthesizer::clearOldActions(uint32_t now)
{
    _mutex.lock();
        for (std::size_t i=0; i<_actions.size(); ++i)
        {
            if (_actions[i].content()["realtimestamp"]->asLongLong() < now)
            {
                _actions.erase(_actions.begin()+i);
                i--;
            }
            else
            {
                break;
            }
        }
    _mutex.unlock();
}

void ActionSynthesizer::clearAllActions()
{
    _mutex.lock();
        _actions.clear();
    _mutex.unlock();
}

void ActionSynthesizer::clearAllSimulations()
{
    _mutex.lock();
        _clearSimulations = true;
    _mutex.unlock();
}

void ActionSynthesizer::start()
{
    bool working = false;
    _mutex.lock();
        working = _working;
    _mutex.unlock();
        
    if (!working)
    {
        _working = true;
        _thread = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&ActionSynthesizer::worker, shared_from_this())));
    }
    else
    {
        std::cerr << "ActionSynthesizer: Cannot start thread: already running" << std::endl;
    }
}

void ActionSynthesizer::stop()
{
	_working = false;
	if (_thread)
	{
		_thread->join();
	}
}

void ActionSynthesizer::worker()
{
    while (_working)
    {
        bool clearSimulations = false;
        _mutex.lock();
            clearSimulations = _clearSimulations;
            _clearSimulations = false;
        _mutex.unlock();
        
        if (clearSimulations)
        {
            std::vector<boost::uuids::uuid> toRemove;
            for (std::size_t i=0; i<_simulations.size(); ++i)
            {
                toRemove.push_back(_simulations[i]->getId());
            }
            _simulations.clear();
            for (std::size_t i=0; i<toRemove.size(); ++i)
            {
                SimulationEngine::getInstance()->removeSimulationNow(toRemove[i]);
            }
            
            _clearSimulations = false;
        }
        
        _threadActions.clear();
        _mutex.lock();
            for (std::size_t i=0; i<_actions.size(); ++i)
            {
                _threadActions.push_back(_actions[i].copy());
            }
        _mutex.unlock();
        
        
        bool actionsChanged = false;
        
        //loop through all and select a best one
        double bestScore = 0.0;
        boost::shared_ptr<Simulation> bestSimulation;
        std::vector<boost::uuids::uuid> toRemove;
        for (unsigned int i=0; i<_simulations.size(); ++i)
        {
            if ((_simulations[i]->isRunning()) && !(_simulations[i]->isInvalid())) continue;
            
            if (!_simulations[i]->isInvalid())
            {
                if (_simulations[i]->getGoal() && (_simulations[i]->getGoal()->getSatisfaction() > bestScore))
                {
                    bestScore = _simulations[i]->getGoal()->getSatisfaction();
                    bestSimulation = _simulations[i];
                }
            }
            
            toRemove.push_back(_simulations[i]->getId());
            
            //remove from private list, they are not needed anymore
            _simulations.erase(_simulations.begin()+i);
            i--;
        }
        
        //apply policy on the best one
        if (bestSimulation)
        {
            //apply policy to modify future action sequence
            actionsChanged = _policy->apply(bestSimulation, _tag, _threadActions);
        }
        
        //remove all in the list
        for (std::size_t i=0; i<toRemove.size(); ++i)
        {
            //dispose of simulation
            SimulationEngine::getInstance()->removeSimulationNow(toRemove[i]);
        }
        
        if (actionsChanged)
        {
            for (unsigned int i=0; i<_simulations.size(); ++i)
            {
                _simulations[i]->setInvalid(true);
            }
        }
        
        _mutex.lock();
            if (!_actionsSubmittedFromMainThread)
            {
                _actions.clear();
                for (std::size_t i=0; i<_threadActions.size(); ++i)
                {
                    _actions.push_back(_threadActions[i].copy());
                }
            }
            _actionsSubmittedFromMainThread = false;
        _mutex.unlock();
        
        
        boost::this_thread::sleep_for(boost::chrono::milliseconds(5));
    }
}

}
}
