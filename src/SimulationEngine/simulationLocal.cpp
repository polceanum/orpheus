//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#include "SimulationEngine/simulationLocal.h"

#include <boost/uuid/uuid_io.hpp>

#include <iomanip>      // std::setprecision

namespace orpheus
{
namespace simulation_engine
{

SimulationLocal::SimulationLocal(boost::shared_ptr<orpheus::metamodel::MentalImage> img) : Simulation()
{
	_working = false;
    //_stepSize = 0.01;
    _stepSize = 0.1;
    _totalRunTime = 0.0;
    _maxRunTime = 0.0;
    _subscriberFeedInterval = 20.0; //20ms default
    _simulationImage = img->copy();
    _modelsInitalized = false;
    
    //debug
    _debugProcessingTimeOutput = false;
}

SimulationLocal::~SimulationLocal()
{
	destroy();
}

void SimulationLocal::start()
{
    bool working = false;
	_mutex.lock();
        working = _working;
    _mutex.unlock();
    
    if (!working)
    {
        _working = true;
        _simulationLocalStartTime = boost::posix_time::microsec_clock::local_time();
        _thread = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&SimulationLocal::worker, shared_from_this())));
    }
    else
    {
        std::cerr << "SimulationLocal: Cannot start thread: already running" << std::endl;
    }
}

void SimulationLocal::stop()
{
	_mutex.lock();
		_working = false;
	_mutex.unlock();
	if (_thread)
	{
		_thread->join();
	}
}

void SimulationLocal::destroy()
{
	stop();
	for (int i=0; i<_models.size(); ++i)
	{
		_models[i]->erase();
	}
	//_models.clear();
    
    //std::cerr << "Destroying simulation. Total run time = " << getTotalRunTime() << std::endl;
    
    //_totalRunTime = 0.0;
}

void SimulationLocal::setMaxRunTime(double seconds)
{
    _mutex.lock();
        _maxRunTime = seconds;
    _mutex.unlock();
}

double SimulationLocal::getMaxRunTime()
{
    double maxTime = 0.0;
    _mutex.lock();
        maxTime = _maxRunTime;
    _mutex.unlock();
    return maxTime;
}

void SimulationLocal::setStepSize(double s)
{
    _mutex.lock();
        _stepSize = s;
    _mutex.unlock();
}

boost::shared_ptr<orpheus::metamodel::MentalImage> SimulationLocal::getMentalImage()
{
    boost::shared_ptr<orpheus::metamodel::MentalImage> img;
    _mutex.lock();
        img = _simulationImage->copy();
    _mutex.unlock();
    
    return img;
}

double SimulationLocal::getTotalRunTime()
{
    double runTime;
    _mutex.lock();
        runTime = _totalRunTime;
    _mutex.unlock();
    return runTime;
}

void SimulationLocal::setZeroTime(uint32_t time)
{
    _mutex.lock();
        _simulationImage->setZeroTime(time);
    _mutex.unlock();
}

uint32_t SimulationLocal::getZeroTime()
{
    uint32_t time;
    _mutex.lock();
        time = _simulationImage->getZeroTime();
    _mutex.unlock();
    return time;
}

bool SimulationLocal::isRunning()
{
    bool running = true;
    if (_mutex.try_lock())
    {
        running = _working;
        _mutex.unlock();
    }
    return running;
}

void SimulationLocal::addSimulationController(boost::shared_ptr<SimulationController> sc)
{
    _mutex.lock();
        _simulationControllers.push_back(sc);
    _mutex.unlock();
}

std::vector<boost::shared_ptr<SimulationController> > SimulationLocal::getSimulationControllers() //not thread safe
{
    return _simulationControllers;
}

std::vector<orpheus::value_specifications::ValueBag> SimulationLocal::getSimulationControllerHistory(std::string tag)
{
    std::vector<orpheus::value_specifications::ValueBag> history;
    for (int i=0; i<_simulationControllers.size(); ++i)
    {
        if (_simulationControllers[i]->getTag() == tag)
        {
            _mutex.lock();
                history = _simulationControllers[i]->getParameterHistory();
            _mutex.unlock();
            break;
        }
    }
    
    return history;
}

void SimulationLocal::addModel(boost::shared_ptr<orpheus::models::Model> model) //not thread safe !
{
    _models.push_back(model);
    //model->initialize();
    //model->initFromImage(_simulationImage);
}

boost::shared_ptr<orpheus::models::Model> SimulationLocal::getModelsFor(boost::uuids::uuid id) //not thread safe !
{
    //boost::shared_ptr<orpheus::models::Model> result;
    for (int i=0; i<_models.size(); ++i)
    {
        orpheus::value_specifications::ValueBag &params = _models[i]->accessParameters();
        if (params.content().find("gravity") != params.content().end()) continue; //TESTING. Skip physics for now
        
        orpheus::models::ModelPermissions permissions = _models[i]->accessPermissions();
        
        if (permissions.hasPermission(id))
        {
            //result.push_back(_models[i]->copy());
            return _models[i]->copy();
        }
    }
    //return result;
    return boost::shared_ptr<orpheus::models::Model>();
}

void SimulationLocal::setSubscriberFeedInterval(long long ms)
{
    _mutex.lock();
        _subscriberFeedInterval = ms;
    _mutex.unlock();
}

void SimulationLocal::worker()
{
    if (!_modelsInitalized)
    {
        for (int i=0; i<_models.size(); ++i)
        {
            _models[i]->initialize();
            _models[i]->initFromImage(_simulationImage);
        }
        _modelsInitalized = true;
    }
    
	while (true)
	{
		_mutex.lock();
        
        //std::cerr << std::setprecision(10) << "Timediff: " << _maxRunTime-_totalRunTime << "/" << FLT_EPSILON*10 << std::endl;
        
        if(_working && ((_maxRunTime == 0.0) || (_maxRunTime-_totalRunTime > 0.00001)))
		{
            boost::shared_ptr<orpheus::metamodel::MentalImage> backupSim = _simulationImage->copy();
            
            double correctStepSize = _stepSize;
            
            if (_maxRunTime-_totalRunTime < _stepSize)
            {
                correctStepSize = _maxRunTime-_totalRunTime;
            }
            
			for (int i=0; i<_models.size(); ++i)
			{
                _models[i]->step(correctStepSize, backupSim, _simulationImage);
				//std::cerr << "stepping model " << i << std::endl;
			}
            
            _simulationImage->advanceVirtualTime((uint32_t)(correctStepSize*1000)); //advance image virtual time
            _totalRunTime += correctStepSize;
            
            boost::posix_time::ptime now = boost::posix_time::microsec_clock::local_time();
            boost::posix_time::time_duration diff = (now-_lastSubscriptionFeed);
            if ((diff.total_milliseconds() > _subscriberFeedInterval) || (correctStepSize != _stepSize))
            {
                _feedImageToSubscribers(_simulationImage);
                _lastSubscriptionFeed = now;
            }
            
            //mutate model parameters -- still testing...
            //if (_actionsAllowed()) std::cerr << "actions are now allowed !" << std::endl;
            if (_totalRunTime > correctStepSize) //if it's not the first step, if we have a controller, and if actions are allowed
            {
                for (int i=0; i<_simulationControllers.size(); ++i)
                {
                    //apply novel actions
                    _simulationControllers[i]->mutateModelParameters(_simulationImage, _actionsAllowed()); //controller takes care of transferred actions, if allowed
                }
                _controllerTime = _totalRunTime;
            }
            
            //test goal satisfaction
            if (_goal)
            {
                _goal->update(_simulationImage);
                
                if (_goal->requestsAbortSimulation())
                {
                    _working = false;
                    _mutex.unlock(); //don't forget to leave the door open !
                    break; //stops thread
                }
            }
		}
		else
		{
            _working = false;
			_mutex.unlock(); //don't forget to leave the door open !
			break; //stops thread
		}
        /*
        boost::posix_time::ptime now = boost::posix_time::microsec_clock::local_time();
        boost::posix_time::time_duration diff = (now-_simulationImage->getZeroTime());
        std::cerr << "Estimated simulation time ratio: " << (diff.total_milliseconds()/1000.0)/(_totalRunTime) << " at " << _simulationImage->getPassedMillis()/1000.0 << std::endl;
        std::cerr << (diff.total_milliseconds()/1000.0)/(_totalRunTime) << " vs " << (_simulationImage->getPassedMillis()/1000.0) << std::endl;
        */
        //boost::this_thread::sleep(boost::posix_time::milliseconds(5)); //a lot of time to spend... fix later
        boost::this_thread::sleep_for(boost::chrono::milliseconds(5));
        
		_mutex.unlock();
	}
    
    boost::posix_time::time_duration diff = (boost::posix_time::microsec_clock::local_time()-_simulationLocalStartTime);
    _totalProcessingTime = diff.total_milliseconds()/1000.0;
    
    if (_debugProcessingTimeOutput)
    {
        std::cerr << "Finished simulation: " << _totalRunTime << "s/" << _maxRunTime << "s in real: " << _totalProcessingTime << "s" << std::endl;
        //std::cerr << "Estimated simulation time ratio: " << (diff.total_milliseconds()/1000.0)/(_totalRunTime) << std::endl;
    }
}

bool SimulationLocal::_actionsAllowed()
{
    //this will return false (and therefore inhibit actions) as long as the estimated processing time is greater than the current virtual time
    
    double timeToWait = _getTimeBeforeActions();
    
    if (timeToWait > 0.0)
    {
        return (timeToWait <= (_simulationImage->getPassedMillis()/1000.0));
    }
    
    return false;
}

double SimulationLocal::_getTimeBeforeActions()
{
    boost::posix_time::ptime now = boost::posix_time::microsec_clock::local_time();
    boost::posix_time::time_duration diff = (now-_simulationLocalStartTime);
    
    double estimatedTotalRuntime = (_maxRunTime*(diff.total_milliseconds()/1000.0)/(_totalRunTime))*1.1;
    
    //std::cerr << "Estimated simulation time ratio: " << (diff.total_milliseconds()/1000.0)/(_totalRunTime) << " at " << _simulationImage->getPassedMillis()/1000.0 << std::endl;
    //std::cerr << "Estimated total runtime: " << estimatedTotalRuntime << " at " << _simulationImage->getPassedMillis()/1000.0 << std::endl;
    
    return estimatedTotalRuntime;
}

void SimulationLocal::setDebugProcessingTimeOutput(bool dbg)
{
    _debugProcessingTimeOutput = dbg;
}

}
}
