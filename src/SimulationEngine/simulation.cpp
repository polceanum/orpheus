//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#include "SimulationEngine/simulation.h"

namespace orpheus
{
namespace simulation_engine
{

Simulation::Simulation() : _simulationId(boost::uuids::random_generator()()), _totalProcessingTime(0.0), _invalid(false)
{
    
}

Simulation::~Simulation()
{

}

void Simulation::addSubscriber(boost::shared_ptr<orpheus::communication::tcp_client> client)
{
    _subscriptionMutex.lock();
        _subscribers.push_back(client);
    _subscriptionMutex.unlock();
}

bool Simulation::hasSubscriber(boost::shared_ptr<orpheus::communication::tcp_client> client)
{
    bool has = false;
    _subscriptionMutex.lock();
        std::vector<boost::shared_ptr<orpheus::communication::tcp_client> >::iterator it;
        it = std::find(_subscribers.begin(), _subscribers.end(), client);
        if (it != _subscribers.end())
        {
            has = true;
        }
    _subscriptionMutex.unlock();
    return has;
}

void Simulation::removeSubscriber(boost::shared_ptr<orpheus::communication::tcp_client> client)
{
    _subscriptionMutex.lock();
        std::vector<boost::shared_ptr<orpheus::communication::tcp_client> >::iterator it;
        it = std::find(_subscribers.begin(), _subscribers.end(), client);
        if (it != _subscribers.end())
        {
            _subscribers.erase(it);
        }
    _subscriptionMutex.unlock();
}

void Simulation::transferSubscriptionsFrom(boost::shared_ptr<Simulation> sim)
{
    _subscriptionMutex.lock();
    sim->_subscriptionMutex.lock();
            _subscribers = sim->_subscribers;
    sim->_subscriptionMutex.unlock();
    _subscriptionMutex.unlock();
    
    //std::cerr << "transferred " << _subscribers.size() << " clients" << std::endl;
}

double Simulation::getTotalProcessingTime() const //how much time did it really take to simulate ?
{
    return _totalProcessingTime;
}

void Simulation::_feedImageToSubscribers(boost::shared_ptr<orpheus::metamodel::MentalImage> img)
{
    _subscriptionMutex.lock();
    if (_subscribers.size() > 0)
    {
        std::string imgStr = img->toString()+"\n";
        if (imgStr != "\n")
        {
            for (int i=0; i<_subscribers.size(); ++i)
            {
                if (!_subscribers[i]->is_connected())
                {
                    std::cerr << "Lost connection to " << _subscribers[i]->getHost() << ":" << _subscribers[i]->getPort() << " ... unsubscribing." << std::endl;
                    _subscribers.erase(_subscribers.begin()+i);
                    i--;
                    continue;
                }
                _subscribers[i]->send(imgStr);
            }
        }
    }
    _subscriptionMutex.unlock();
}

void Simulation::setInvalid(bool i)
{
    _invalid = i;
}

bool Simulation::isInvalid()
{
    return _invalid;
}

bool Simulation::isFinished()
{
    return ((getMaxRunTime()-getTotalRunTime() <= 0.00001) && !isRunning());
}

void Simulation::setId(boost::uuids::uuid id)
{
    _simulationId = id;
}

boost::uuids::uuid Simulation::getId()
{
    return _simulationId;
}

void Simulation::setGoal(boost::shared_ptr<orpheus::goal_system::Goal> goal)
{
    _goal = goal;
}

boost::shared_ptr<orpheus::goal_system::Goal> Simulation::accessGoal()
{
    return _goal;
}

boost::shared_ptr<const orpheus::goal_system::Goal> Simulation::getGoal() const
{
    return _goal;
}

}
}
