//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#include "SimulationEngine/simulationController.h"

#include "ValueSpecifications/valueSpecification.h"

namespace orpheus
{
namespace simulation_engine
{

SimulationController::SimulationController(std::string tag) : _tag(tag)
{
    
}

SimulationController::~SimulationController()
{
    
}

std::string SimulationController::getTag() const
{
    return _tag;
}

std::vector<orpheus::value_specifications::ValueBag> SimulationController::getParameterHistory()
{
    return _parameterHistory;
}

void SimulationController::transferParameterHistory(std::vector<orpheus::value_specifications::ValueBag> hist, uint32_t startTime)
{
    _transferredParameterHistory.clear();
    for (int i=0; i<hist.size(); ++i)
    {
        if (hist[i].content()["realtimestamp"]->asLongLong() >= startTime)
        {
            _transferredParameterHistory.push_back(hist[i].copy());
        }
    }
}

}
}
