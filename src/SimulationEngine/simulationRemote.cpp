//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#include "SimulationEngine/simulationRemote.h"

#include "Communication/communicationManager.h"

#include "Tools/base64.h"

#include <boost/uuid/uuid_io.hpp>
#include <boost/tokenizer.hpp>

namespace orpheus
{
namespace simulation_engine
{

SimulationRemote::SimulationRemote(boost::shared_ptr<orpheus::metamodel::MentalImage> img, std::string ip, std::string port) : Simulation(), _host(ip), _port(port)
{
    _slaveClient = orpheus::communication::CommunicationManager::getInstance(_host+":"+_port)->getClient(ip, port);
    _simulationImage = img->copy();
    _goalAchieved = false;
    _working = false;
}

SimulationRemote::~SimulationRemote()
{
    
}

void SimulationRemote::start()
{
	//boost::posix_time::ptime now = boost::posix_time::microsec_clock::local_time();
    //_simulationImage->setZeroTime(now);
    
    /*
    //devel stuff
    boost::posix_time::ptime epoch(boost::gregorian::date(1970,1,1));
    long long nowMillis = (now - epoch).total_milliseconds();
    std::cerr << "Will send remote simulation with timestamp: " << nowMillis << std::endl;
    */
    
    std::string requestString = "Request:BindStrangeSimulation";
    requestString += "."+boost::lexical_cast<std::string>(_simulationId);
    requestString += "."+_simulationImage->toString();
    
    _working = true;
    
    _slaveClient->send(requestString+":"+orpheus::communication::CommunicationManager::getInstance(_host+":"+_port)->getServer()->getPort()+"\n");
}

void SimulationRemote::stop()
{
	
}

void SimulationRemote::destroy()
{
	
}

void SimulationRemote::setMaxRunTime(double seconds)
{
    
}

double SimulationRemote::getMaxRunTime()
{
    return 0.0; //dummy
}

void SimulationRemote::setZeroTime(uint32_t time)
{
    _simulationImage->setZeroTime(time);
}

void SimulationRemote::setStepSize(double s)
{
    
}

boost::shared_ptr<orpheus::metamodel::MentalImage> SimulationRemote::getMentalImage()
{
    boost::shared_ptr<orpheus::metamodel::MentalImage> img = _simulationImage->copy();
    
    return img;
}

double SimulationRemote::getTotalRunTime()
{
    return 0.0;//_totalRunTime;
}

bool SimulationRemote::isRunning()
{
    return _working;
}

void SimulationRemote::setSimulationController(boost::shared_ptr<SimulationController> sc)
{
    //_simulationController = sc;
}

std::vector<orpheus::value_specifications::ValueBag> SimulationRemote::getSimulationControllerHistory()
{
    return _parameterHistory;
}

void SimulationRemote::setGoal(boost::shared_ptr<orpheus::goal_system::Goal> goal)
{
    _goal = goal;
}

boost::shared_ptr<orpheus::goal_system::Goal> SimulationRemote::accessGoal()
{
    return _goal;
}

boost::shared_ptr<orpheus::models::Model> SimulationRemote::getModelsFor(boost::uuids::uuid id) //not thread safe !
{
    return boost::shared_ptr<orpheus::models::Model>();
}

void SimulationRemote::setFinished()
{
    _working = false;
    
    //devel stuff
    /*
    boost::posix_time::ptime now = boost::posix_time::microsec_clock::local_time();
    boost::posix_time::ptime epoch(boost::gregorian::date(1970,1,1));
    long long nowMillis = (now - epoch).total_milliseconds();
    std::cerr << "Received simulation result at time: " << nowMillis << " -- " << getId() << std::endl;
    */
}

void SimulationRemote::setGoalAchieved(bool g)
{
    _goal->setSatisfaction((g)?1.0:0.0); //TODO: improve later
}

void SimulationRemote::deserializeSimulationControllerHistory(std::string &str)
{
    std::vector<orpheus::value_specifications::ValueBag> params;
    
    const std::string &decodedString = base64_decode(str);
    boost::char_separator<char> sep(":");
    boost::tokenizer<boost::char_separator<char> > tokens(decodedString, sep);
    for (boost::tokenizer<boost::char_separator<char> >::iterator tok_iter = tokens.begin(); tok_iter!=tokens.end(); ++tok_iter)
    {
        const std::string token = (*tok_iter);
        
        params.push_back(orpheus::value_specifications::ValueBag::fromString(token));
    }
    
    _parameterHistory = params;
}

}
}
