//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#include "ModelAssignment/modelAssignment.h"

//#include "Models/Physics/bulletSimulation.h"

namespace orpheus
{
namespace model_assignment
{

boost::shared_ptr<ModelAssignment> ModelAssignment::_instance;

ModelAssignment::~ModelAssignment()
{
    
}

boost::shared_ptr<ModelAssignment> ModelAssignment::getInstance()
{
    if (!_instance)
	{
		_instance = boost::shared_ptr<ModelAssignment>(new ModelAssignment());
	}
	
	return _instance;
}

void ModelAssignment::learnObjectModelAssociations(std::vector<boost::uuids::uuid> transferred, boost::shared_ptr<orpheus::simulation_engine::Simulation> sim)
{
    for (size_t i=0; i<transferred.size(); ++i)
    {
        //for now do direct association
        boost::shared_ptr<orpheus::models::Model> model = sim->getModelsFor(transferred[i]);
        if (model)
        {
            _idToModel[transferred[i]] = model->copy();
        }
        else
        {
            _idToModel[transferred[i]] = boost::shared_ptr<orpheus::models::Model>(); //null
        }
    }
}

void ModelAssignment::assignModelsToSimulation(boost::shared_ptr<orpheus::simulation_engine::SimulationLocal> sim) //TODO: to be changed to generic Simulation
{
    boost::shared_ptr<orpheus::metamodel::MentalImage> img = sim->getMentalImage();
    /*
    //create physics model
    boost::shared_ptr<orpheus::models::BulletSimulation> physics = boost::shared_ptr<orpheus::models::BulletSimulation>(new orpheus::models::BulletSimulation());
    physics->accessPermissions().grantAll(img);
    //physics->accessParameters()["gravity"] = -10+((double)rand()/RAND_MAX)*20;
    physics->applyParameters();
    sim->addModel(physics);
    */
    
    //not used for now
}

/*
std::vector<boost::uuids::uuid> ModelAssignment::adjustToCorrespondWith(boost::shared_ptr<const ModelAssignment> img)
{
    double cMatrix[size()][img->size()];
    int match[size()];
    double matchDist[size()];
    
    for (int i=0; i<size(); ++i)
    {
        double minDist = -1;
        for (int j=0; j<img->size(); ++j)
        {
            cMatrix[i][j] = this->getObject(i).differenceTo(img->getObject(j));
            if ((minDist < 0) || (cMatrix[i][j] < minDist))
            {
                match[i] = j;
                minDist = cMatrix[i][j];
                matchDist[i] = cMatrix[i][j];
            }
        }
    }
    
    //mean
    double meanDifference = 0.0;
    for (int i=0; i<size(); ++i)
    {
        meanDifference += matchDist[i];
    }
    meanDifference /= size();
    
    //variation
    double variation = 0.0;
    for (int i=0; i<size(); ++i)
    {
        double diftomean = matchDist[i]-meanDifference;
        variation += (diftomean*diftomean);
    }
    variation /= size();
    
    //standard deviation
    double sigma = sqrt(variation);
    double trustedThreshold = meanDifference+2*sigma;
    
    bool duplicate[size()];
    for (int i=0; i<size(); ++i) duplicate[i] = false;
    int duplicates = 0;
    for (int i=0; i<size()-1; ++i)
    {
        for (int j=i+1; j<size(); ++j)
        {
            if (match[i] == match[j])
            {
                duplicates++;
                duplicate[i] = true;
            }
            
        }
    }
    //std::cerr << "Duplicates: " << duplicates << std::endl;
    
    std::vector<boost::uuids::uuid> transferedIDs;
    //transfer object IDs
    int nrObjsMatched = 0;
    for (int i=0; i<size(); ++i)
    {
        if (!duplicate[i] && (matchDist[i] < trustedThreshold))
        {
            //std::cerr << match[i] << "/" << img->size() << std::endl;
            replaceObjectId(i, img->getObject(match[i]).id);
            nrObjsMatched++;
            transferedIDs.push_back(img->getObject(match[i]).id);
        }
    }
    std::cerr << "matched " << nrObjsMatched << "/" << size() << " objects" << std::endl;
    
    return transferedIDs;
}
*/

ModelAssignment::ModelAssignment() : _generator(time(0))
{
	
}

}
}
