```
 O.R.P.H.E.U.S.:
 Reasoning and
 Prediction with
 Heterogeneous
rEpresentations
 Using
 Simulation
```

# README #

Basic info on how to get it to work.

### What is this repository for? ###

* Having my code nice and tidy !
* Being useful to projects implementing agents with prediction capabilities.

* * *
# Setting it up #
* * *

### 1. Getting the code ###

* Create a folder where you wish to place the files, then clone the repository.

```
#!bash
mkdir ORPHEUS
cd ORPHEUS
git clone https://bitbucket.org/polceanum/orpheus.git .
```

* * *

### 2. Building the library ###

* ORPHEUS requires the following libraries to build:
    * [Eigen](http://eigen.tuxfamily.org/) (tested with v3.0.5)
    * [Boost](http://www.boost.org/) (tested with v1.54.0) :: links with **boost_system**, **boost_thread**

* Create a build directory and call cmake and then make. Example:
```
#!bash
cd ORPHEUS
mkdir build
cd build
cmake ..
make
```

* This will build a dynamic library: **libORPHEUS.so**

* You can link this library into your own project to use its functionality.

* * *

### 3. Trying it out ###

* Demos are set up separately from the main library. They can be accessed here:

    https://bitbucket.org/polceanum/orpheus.demos

* * *

# Open Source License #

* ORPHEUS is released under the GNU LGPL Open Source license. Please read the LICENSE file distributed with the library.

# Commercial License #

* Commercial use has not been thoroughly discussed yet but if interested, please directly contact the authors.