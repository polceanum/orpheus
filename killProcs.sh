#!/bin/sh

#ps -A | grep -ie orpheus | awk '{print $1}' | xargs kill -9

PID=`ps -A | grep -ie orpheus | awk '{print $1}'`

if [ -z "${PID}" ]
then
echo "No process to kill."
else
kill -9 ${PID}
echo "Killed processes:\n${PID}"
fi
