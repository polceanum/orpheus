//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#ifndef ORPHEUS_SIMULATION_LOCAL_H
#define ORPHEUS_SIMULATION_LOCAL_H

#include "SimulationEngine/simulation.h"
#include "Models/model.h"
#include "Metamodel/mentalImage.h"

#include <boost/thread/thread.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <vector>

namespace orpheus
{
namespace simulation_engine
{

class SimulationLocal : public boost::enable_shared_from_this<SimulationLocal>, public Simulation
{
public:
    SimulationLocal(boost::shared_ptr<orpheus::metamodel::MentalImage>); //create from perceptual image
	virtual ~SimulationLocal();
	
    //------------------------------------------------------------------//
	virtual void start();
	virtual void stop();
	virtual void destroy();
	
    virtual void setMaxRunTime(double); //seconds
    virtual double getMaxRunTime();
    virtual void setZeroTime(uint32_t);
    virtual uint32_t getZeroTime();
    
    virtual void setStepSize(double);
    
    virtual boost::shared_ptr<orpheus::metamodel::MentalImage> getMentalImage(); //provide the current perceptual image of this simulation
    virtual double getTotalRunTime();
    
    virtual bool isRunning(); //whether or not the thread is working
    
    virtual void addSimulationController(boost::shared_ptr<SimulationController>);
    virtual std::vector<boost::shared_ptr<SimulationController> > getSimulationControllers();
    virtual std::vector<orpheus::value_specifications::ValueBag> getSimulationControllerHistory(std::string tag);
    
    boost::shared_ptr<orpheus::models::Model> getModelsFor(boost::uuids::uuid);
    //------------------------------------------------------------------//
    
    void setDebugProcessingTimeOutput(bool);
    
    void addModel(boost::shared_ptr<orpheus::models::Model>);
    
    virtual void setSubscriberFeedInterval(long long ms); //number of milliseconds between each frame sent to subscribers
	
protected:
	virtual void worker(); //method that gets sent to the thread. all logic in here
    
    bool _actionsAllowed();
    virtual double _getTimeBeforeActions();

	//main-only variables
	boost::shared_ptr<boost::thread> _thread;
	
	//cross-thread variables
	bool _working;
	boost::mutex _mutex;
    double _stepSize;
    double _totalRunTime;
    double _maxRunTime;
    boost::shared_ptr<orpheus::metamodel::MentalImage> _simulationImage;
    long _subscriberFeedInterval;
    std::vector<boost::shared_ptr<SimulationController> > _simulationControllers;
	
	//thread-only variables
    std::vector<boost::shared_ptr<orpheus::models::Model> > _models;
    bool _modelsInitalized;
    boost::posix_time::ptime _lastSubscriptionFeed;
    boost::posix_time::ptime _simulationLocalStartTime;
    double _controllerTime;
    
    //debug
    bool _debugProcessingTimeOutput;
	
private:
	
};

}
}

#endif
