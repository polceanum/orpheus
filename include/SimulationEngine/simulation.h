//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#ifndef ORPHEUS_SIMULATION_H
#define ORPHEUS_SIMULATION_H

#include "Metamodel/mentalImage.h"
#include "SimulationEngine/simulationController.h"
#include "Communication/tcp_client.h"
#include "Models/model.h"
#include "GoalSystem/goal.h"
#include <string>

namespace orpheus
{
namespace simulation_engine
{

// <<interface>>
class Simulation
{
public:
	Simulation();
	virtual ~Simulation();
	
    //------------------------------------------------------------------//
	virtual void start()=0;
	virtual void stop()=0;
	virtual void destroy()=0;
    
    virtual void setMaxRunTime(double)=0;
    virtual double getMaxRunTime()=0;
    virtual void setZeroTime(uint32_t) = 0;
    virtual uint32_t getZeroTime() = 0;
	
    virtual void setStepSize(double) = 0;
    
    virtual boost::shared_ptr<orpheus::metamodel::MentalImage> getMentalImage()=0; //provide the current perceptual image of this simulation
    virtual double getTotalRunTime()=0; //get how many seconds the simulation has advanced since its creation
    
    virtual bool isRunning()=0; //whether or not the thread is working
    
    virtual void addSimulationController(boost::shared_ptr<SimulationController>)=0;
    virtual std::vector<boost::shared_ptr<SimulationController> > getSimulationControllers()=0;
    virtual std::vector<orpheus::value_specifications::ValueBag> getSimulationControllerHistory(std::string tag) = 0;
    
    virtual void setGoal(boost::shared_ptr<orpheus::goal_system::Goal>);
    virtual boost::shared_ptr<orpheus::goal_system::Goal> accessGoal();
    virtual boost::shared_ptr<const orpheus::goal_system::Goal> getGoal() const;
    
    virtual boost::shared_ptr<orpheus::models::Model> getModelsFor(boost::uuids::uuid)=0;
    //------------------------------------------------------------------//
    
    void setInvalid(bool);
    bool isInvalid();
    
    virtual bool isFinished(); //if thread is not working and time is up
    
    void setId(boost::uuids::uuid);
    boost::uuids::uuid getId();
    
    //visualization
    void addSubscriber(boost::shared_ptr<orpheus::communication::tcp_client> client);
    bool hasSubscriber(boost::shared_ptr<orpheus::communication::tcp_client> client);
    void removeSubscriber(boost::shared_ptr<orpheus::communication::tcp_client> client);
    void transferSubscriptionsFrom(boost::shared_ptr<Simulation> sim);
    
    //report
    double getTotalProcessingTime() const; //how much time did it really take to simulate ?
	
protected:
    bool _invalid;
    
    void _feedImageToSubscribers(boost::shared_ptr<orpheus::metamodel::MentalImage>);
    
    boost::uuids::uuid _simulationId; //unique simulation identifier
    
    double _totalProcessingTime;
    
    boost::shared_ptr<orpheus::goal_system::Goal> _goal;
    
private:
    std::vector<boost::shared_ptr<orpheus::communication::tcp_client> > _subscribers;
    boost::mutex _subscriptionMutex;
	
};

}
}

#endif
