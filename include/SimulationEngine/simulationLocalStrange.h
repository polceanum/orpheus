//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#ifndef ORPHEUS_SIMULATION_LOCAL_STRANGE_H
#define ORPHEUS_SIMULATION_LOCAL_STRANGE_H

#include "SimulationEngine/simulationLocal.h"
#include "Models/model.h"
#include "Metamodel/mentalImage.h"

#include <boost/thread/thread.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <vector>

namespace orpheus
{
namespace simulation_engine
{

class SimulationLocalStrange : public SimulationLocal
{
public:
    SimulationLocalStrange(boost::uuids::uuid, boost::shared_ptr<orpheus::metamodel::MentalImage>, boost::shared_ptr<orpheus::communication::tcp_client>); //create from perceptual image
	virtual ~SimulationLocalStrange();
    
    //------------------------------------------------------------------//
    virtual void start();
    //------------------------------------------------------------------//
	
	
protected:
	virtual void worker(); //method that gets sent to the thread. all logic in here
    virtual double _getTimeBeforeActions();
	
private:
	boost::shared_ptr<orpheus::communication::tcp_client> _masterClient;
    long long _requestTransferTime;
};

}
}

#endif
