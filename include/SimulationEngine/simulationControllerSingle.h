//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#ifndef ORPHEUS_SIMULATION_CONTROLLER_SINGLE_H
#define ORPHEUS_SIMULATION_CONTROLLER_SINGLE_H

#include "SimulationEngine/simulationController.h"

namespace orpheus
{
namespace simulation_engine
{

class SimulationControllerSingle : public SimulationController
{
public:
	SimulationControllerSingle(std::string tag, boost::shared_ptr<orpheus::models::Model> target);
	virtual ~SimulationControllerSingle();
    
    void mutateModelParameters(boost::shared_ptr<const orpheus::metamodel::MentalImage>, bool actionsAllowed);
    
    //------------------------------------------------------------------------------//
    //return true if history should be kept for the current model
    //i.e. true = keep mutation history -- false = don't keep history -- History is required to replay actions in the real world !
    virtual bool mutateParameterHook(boost::shared_ptr<const orpheus::metamodel::MentalImage>) = 0; //called by the mutateModelParameters in each simulation
    //------------------------------------------------------------------------------//
    
protected:
    boost::shared_ptr<orpheus::models::Model> _targetModel;
    
};

}
}

#endif
