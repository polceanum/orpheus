//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#ifndef ORPHEUS_SIMULATION_CONTROLLER_H
#define ORPHEUS_SIMULATION_CONTROLLER_H

#include "Models/model.h"
#include "Metamodel/mentalImage.h"
#include "ValueSpecifications/valueBag.h"

#include <vector>

namespace orpheus
{
namespace simulation_engine
{

class SimulationController
{
public:
	SimulationController(std::string tag);
	virtual ~SimulationController();
    
    std::string getTag() const;
    
    virtual void mutateModelParameters(boost::shared_ptr<const orpheus::metamodel::MentalImage>, bool actionsAllowed) = 0;
    
    std::vector<orpheus::value_specifications::ValueBag> getParameterHistory();
    
    void transferParameterHistory(std::vector<orpheus::value_specifications::ValueBag>, uint32_t startTime);
    
    //------------------------------------------------------------------------------//
    //return true if history should be kept for the current model
    //i.e. true = keep mutation history -- false = don't keep history -- History is required to replay actions in the real world !
    virtual bool mutateParameterHook(boost::shared_ptr<const orpheus::metamodel::MentalImage>) = 0; //called by the mutateModelParameters in each simulation
    //------------------------------------------------------------------------------//
    
protected:
    std::string _tag;
    std::vector<orpheus::value_specifications::ValueBag> _parameterHistory;
    std::vector<orpheus::value_specifications::ValueBag> _transferredParameterHistory; //actions transferred from previous simulation which are still happening
    
};

}
}

#endif
