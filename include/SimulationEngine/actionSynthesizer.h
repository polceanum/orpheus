//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#ifndef ORPHEUS_ACTION_SYNTHESIZER
#define ORPHEUS_ACTION_SYNTHESIZER

#include "SimulationEngine/simulation.h"
#include "SimulationEngine/synthesizerPolicy.h"

#include <boost/shared_ptr.hpp>
#include <vector>

#include <boost/thread/thread.hpp>

namespace orpheus
{
namespace simulation_engine
{

class ActionSynthesizer : public boost::enable_shared_from_this<ActionSynthesizer>
{
public:
    ActionSynthesizer(std::string tag, boost::shared_ptr<SynthesizerPolicy>);
	virtual ~ActionSynthesizer();
	
    void setActionSequence(std::vector<orpheus::value_specifications::ValueBag> actions, bool abortOthers=true);
    
	void manageSimulation(boost::shared_ptr<Simulation>);
    
    std::vector<orpheus::value_specifications::ValueBag> getActionsToPerform();
    std::size_t getNumberOfActionsToPerform();
    
    void clearOldActions(uint32_t now);
    void clearAllActions();
    void clearAllSimulations();
    
    virtual void start();
	virtual void stop();
	
protected:
    
	
private:
    std::string _tag;
    
    void worker(); //method that gets sent to the thread. most logic in here
    boost::mutex _mutex;
    
    //main-only variables
	boost::shared_ptr<boost::thread> _thread;
	
	//cross-thread variables
	bool _working;
    std::vector<orpheus::value_specifications::ValueBag> _actions;
    bool _actionsSubmittedFromMainThread;
    std::vector<boost::shared_ptr<Simulation> > _simulations;
    bool _clearSimulations;
    
    //thread only
    std::vector<orpheus::value_specifications::ValueBag> _threadActions;
    boost::shared_ptr<SynthesizerPolicy> _policy;
};

}
}

#endif
