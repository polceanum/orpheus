//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#ifndef ORPHEUS_SYNTHESIZER_POLICY
#define ORPHEUS_SYNTHESIZER_POLICY

#include "SimulationEngine/simulation.h"
#include <vector>

namespace orpheus
{
namespace simulation_engine
{

//Policy applied by ActionSynthetizer to modify the future action sequence
class SynthesizerPolicy
{
public:
    SynthesizerPolicy();
	virtual ~SynthesizerPolicy();
	
	virtual bool apply(boost::shared_ptr<Simulation> source, std::string tag, std::vector<orpheus::value_specifications::ValueBag>& actions)=0; //to be implemented by subclasses
	
protected:
    
	
private:
    
};

}
}

#endif
