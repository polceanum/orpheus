//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#ifndef ORPHEUS_SIMULATION_ENGINE
#define ORPHEUS_SIMULATION_ENGINE

#include "SimulationEngine/simulation.h"

#include <boost/shared_ptr.hpp>
#include <vector>

#include <boost/thread/thread.hpp>

namespace orpheus
{
namespace simulation_engine
{

//     <<Singleton>>
class SimulationEngine
{
public:
	virtual ~SimulationEngine();
	
	void addSimulation(boost::shared_ptr<Simulation>);
	void removeSimulation(std::size_t);
    void removeSimulation(boost::shared_ptr<Simulation>);
    void removeSimulation(boost::uuids::uuid);
    void removeSimulationNow(boost::uuids::uuid); //synchronous deletion (bypass garbage collector) to avoid escalation of memory leaks
    boost::shared_ptr<Simulation> accessSimulation(std::size_t);
    boost::shared_ptr<Simulation> accessSimulation(boost::uuids::uuid);
	std::size_t size();
    
	static boost::shared_ptr<SimulationEngine> getInstance();
    
    void addSubscriber(std::size_t simIndex, boost::shared_ptr<orpheus::communication::tcp_client> client);
	
protected:
	
private:
	SimulationEngine();
	static boost::shared_ptr<SimulationEngine> _instance;
	
    boost::mutex _simMutex;
	std::vector<boost::shared_ptr<Simulation> > _simulations;
    
    //std::vector<boost::shared_ptr<orpheus::communication::tcp_client> > _lazySubscribers;
    std::map<std::size_t, std::vector<boost::shared_ptr<orpheus::communication::tcp_client> > > _subscriberLists; //a list for each simulation index
    
    //garbage collection
    boost::shared_ptr<boost::thread> _garbageMan;
    boost::mutex _garbageMutex;
    std::vector<boost::shared_ptr<Simulation> > _garbage;
    void _collectGarbage();
};

}
}

#endif
