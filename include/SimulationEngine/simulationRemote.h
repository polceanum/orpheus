//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#ifndef ORPHEUS_SIMULATION_REMOTE_H
#define ORPHEUS_SIMULATION_REMOTE_H

#include "SimulationEngine/simulation.h"

namespace orpheus
{
namespace simulation_engine
{

class SimulationRemote : public Simulation
{
public:
	SimulationRemote(boost::shared_ptr<orpheus::metamodel::MentalImage>, std::string, std::string);
	virtual ~SimulationRemote();
    
    //------------------------------------------------------------------//
	virtual void start();
	virtual void stop();
	virtual void destroy();
	
    virtual void setMaxRunTime(double); //seconds
    virtual double getMaxRunTime();
    virtual void setZeroTime(uint32_t);
    
    virtual void setStepSize(double);
    
    virtual boost::shared_ptr<orpheus::metamodel::MentalImage> getMentalImage(); //provide the current perceptual image of this simulation
    virtual double getTotalRunTime();
    
    virtual bool isRunning(); //whether or not the thread is working
    
    virtual void setSimulationController(boost::shared_ptr<SimulationController>);
    virtual std::vector<orpheus::value_specifications::ValueBag> getSimulationControllerHistory();
    
    virtual void setGoal(boost::shared_ptr<orpheus::goal_system::Goal>);
    virtual boost::shared_ptr<orpheus::goal_system::Goal> accessGoal();
    
    boost::shared_ptr<orpheus::models::Model> getModelsFor(boost::uuids::uuid);
    //------------------------------------------------------------------//
	
    void setFinished();
    void setGoalAchieved(bool);
    void deserializeSimulationControllerHistory(std::string&);
    
protected:
    boost::shared_ptr<orpheus::goal_system::Goal> _goal;
    
    boost::shared_ptr<orpheus::communication::tcp_client> _slaveClient;
    boost::shared_ptr<orpheus::metamodel::MentalImage> _simulationImage;
    std::vector<orpheus::value_specifications::ValueBag> _parameterHistory;
    bool _goalAchieved;
    bool _working;
    
    std::string _host;
    std::string _port;
	
private:
	
};

}
}

#endif
