//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#ifndef ORPHEUS_METAMODEL_OBJECT
#define ORPHEUS_METAMODEL_OBJECT

#include <boost/shared_ptr.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>

//#include "btBulletDynamicsCommon.h"
#include <eigen3/Eigen/Geometry>

#include <string>

namespace orpheus
{
namespace metamodel
{

class MentalEntity
{
public:
	MentalEntity();
	virtual ~MentalEntity();
	
    boost::uuids::uuid id;
    
    std::string sh; //shape -- TODO replace the data type to something more complete -- currently just a tag
    Eigen::Vector3d shSize; //different stuff depending on shape
    double mass;
    Eigen::Vector3d pos; //position
    Eigen::Quaterniond rot; //rotation
    Eigen::Vector3d lvel; //linear velocity
    Eigen::Vector3d avel; //angular velocity
    Eigen::Vector3d force; //force
    Eigen::Vector3d torque; //torque
    
    //MentalEntity copy();
    std::string toString();
    void fromString(const std::string &);
    
    double differenceTo(const MentalEntity&) const;
	
protected:
	
private:

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

}
}

#endif
