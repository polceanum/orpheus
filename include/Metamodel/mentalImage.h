//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#ifndef ORPHEUS_METAMODEL_PERCEPTUALIMAGE
#define ORPHEUS_METAMODEL_PERCEPTUALIMAGE

#include <boost/shared_ptr.hpp>
#include "boost/date_time/posix_time/posix_time.hpp"
#include <vector>
#include <map>

#include "Metamodel/mentalEntity.h"

namespace orpheus
{
namespace metamodel
{

class MentalImage
{
public:
	MentalImage();
	virtual ~MentalImage();
    
    void setZeroTime(uint32_t);
    uint32_t getZeroTime() const;
    void advanceVirtualTime(uint32_t millis);
    uint32_t getPassedMillis();
    uint32_t getCurrentVirtualTime() const;
    
    void addObject(const MentalEntity &);
    void removeObject(boost::uuids::uuid);
    
    MentalEntity& accessObject(std::size_t);
    MentalEntity& accessObject(boost::uuids::uuid);
    const MentalEntity& getObject(std::size_t) const;
    const MentalEntity& getObject(boost::uuids::uuid) const;
    
    bool contains(boost::uuids::uuid) const;
    
    std::size_t size() const;
    
    boost::shared_ptr<MentalImage> plus(boost::shared_ptr<const MentalImage>) const;
    boost::shared_ptr<MentalImage> minus(boost::shared_ptr<const MentalImage>) const;
    
    //test difference
    void printDiffTo(boost::shared_ptr<const MentalImage>) const;
    void printCorrespondenceTo(boost::shared_ptr<const MentalImage>) const;
    
    std::vector<boost::uuids::uuid> adjustToCorrespondWith(boost::shared_ptr<const MentalImage> img); //returns list of transfered ids
    
    void replaceObjectId(int index, boost::uuids::uuid replacement);
    
    bool hasDuplicates(); //sanity check !
    
    boost::shared_ptr<MentalImage> copy() const;
    
    std::string toString();
    static boost::shared_ptr<MentalImage> fromString(std::string);
	
protected:
	
private:
    std::vector<boost::uuids::uuid> _objectVector;
    std::map<boost::uuids::uuid, MentalEntity, std::less<boost::uuids::uuid>, Eigen::aligned_allocator<std::pair<boost::uuids::uuid, MentalEntity> > > _objectMap;
    uint32_t _zeroTime; //real time to which the first state of the image corresponds
    uint32_t _passedMillis; //number of virtual milliseconds that have passed for this image to arrive in its current state
};

}
}

#endif
