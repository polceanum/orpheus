//helps doxygen to generate relations through shared pointers

namespace boost { template<class T> class shared_ptr { T *shPtr; }; }
namespace std { template<class T> class vector { T *stdVector; }; }
namespace std { template<class T> class list { T *stdList; }; }
namespace std { template<class T> class set { T *stdSet; }; }
namespace std { template<class T, class S> class map { T *mapKey; S *mapElem; }; }
