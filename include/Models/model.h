//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#ifndef ORPHEUS_MODEL_H
#define ORPHEUS_MODEL_H

#include <boost/any.hpp>
#include <string>

#include "ValueSpecifications/valueBag.h"
#include "Models/modelPermissions.h"

namespace orpheus
{
namespace metamodel
{
class MentalImage;
}
namespace value_specifications
{
class ValueBag;
}

namespace models
{

class Model
{
public:
	Model();
	virtual ~Model();
	
    virtual void learn(boost::shared_ptr<const orpheus::metamodel::MentalImage>, boost::shared_ptr<const orpheus::metamodel::MentalImage>, const orpheus::value_specifications::ValueBag&);
    virtual double getPredictionError();
    
    virtual void saveInternalData(); //save model state
    virtual void loadInternalData(); //load model state
    
	virtual void initialize()=0;
	virtual void erase()=0;
    
    void initFromImage(boost::shared_ptr<const orpheus::metamodel::MentalImage>);
    void step(double dt, boost::shared_ptr<const orpheus::metamodel::MentalImage> input, boost::shared_ptr<orpheus::metamodel::MentalImage> result);
    void reset();
    
    void setPermissions(ModelPermissions);
    ModelPermissions& accessPermissions();
    
    //std::map<std::string, boost::any>& accessParameters();
    orpheus::value_specifications::ValueBag& accessParameters();
    const orpheus::value_specifications::ValueBag& getParameters() const;
    bool hasParameter(std::string);
    virtual void applyParameters()=0;
    
    virtual boost::shared_ptr<Model> copy() const=0;
	
protected:
    virtual void step(double)=0;
    virtual void updateFromImage(boost::shared_ptr<const orpheus::metamodel::MentalImage> img)=0;
    virtual void applyStepToImage(boost::shared_ptr<const orpheus::metamodel::MentalImage> input, boost::shared_ptr<orpheus::metamodel::MentalImage> output)=0;
    uint32_t _getVirtualTimeBeforeStep();
    
	
private:
    uint32_t _virtualTimeBeforeStep;
    ModelPermissions _permissions;
    //std::map<std::string, boost::any> _parameters;
    orpheus::value_specifications::ValueBag _parameters;
	
};

}
}

#endif
