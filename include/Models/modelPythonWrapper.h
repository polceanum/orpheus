//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#ifndef ORPHEUS_MODEL_PYTHON_WRAPPER_H
#define ORPHEUS_MODEL_PYTHON_WRAPPER_H

#include "Models/model.h"

#include <boost/python.hpp>
#include <string>
#include <iostream>

namespace orpheus
{
namespace models
{

class ModelPythonWrapper : public Model
{
public:
    ModelPythonWrapper();
	virtual ~ModelPythonWrapper();
    
	virtual void initialize()=0;
	virtual void erase()=0;
    
    virtual void applyParameters()=0;
    
    virtual boost::shared_ptr<Model> copy()=0;
	
protected:
    virtual void step(float)=0;
    virtual void updateFromImage(boost::shared_ptr<const orpheus::metamodel::MentalImage> img)=0;
    virtual void applyStepToImage(boost::shared_ptr<const orpheus::metamodel::MentalImage> input, boost::shared_ptr<orpheus::metamodel::MentalImage> output)=0;
    
    //--------------------------------------------------------------//
    void py_call(std::string);
    //void py_call(std::string, std::map<std::string, float>);
    
    template<class A>
    void py_call(std::string, std::map<std::string, A>);
    
    template<class T>
    T py_rcall(std::string);
    
    //template<class T>
    //T py_rcall(std::string, std::map<std::string, float>);
    
    template<class T, class A>
    T py_rcall(std::string, std::map<std::string, A>);
    
    // <<Singleton>>
    class PythonContextManager
    {
    public:
        virtual ~PythonContextManager();
        
        boost::python::dict createContextFromFile(std::string);
        
        static boost::shared_ptr<PythonContextManager> getInstance();
        
        void print_py_error();
        
        PyInterpreterState* getState() { return _interpreterState; }
        
    private:
        PythonContextManager();
        static boost::shared_ptr<PythonContextManager> _instance;
        boost::python::dict _globalDict;
        boost::python::object _main_module;
        PyThreadState *_state;
        PyInterpreterState* _interpreterState;
    };
        
    class PyExternalUser
    {
    public:
        PyExternalUser(PyInterpreterState* interpreterState) : mInterpreterState(interpreterState),mThreadState(PyThreadState_New(mInterpreterState)) {}
        
        class Use
        {
        public:
            Use(PyExternalUser& user) : mUser(user)
            {
                PyEval_RestoreThread(mUser.getThreadState());
            }
            ~Use()
            {
                mUser.setThreadState(PyEval_SaveThread());
            }
        private:
            PyExternalUser& mUser;
        };
        
        friend class Use;
        
    protected:
        PyThreadState* getThreadState()
        {
            return mThreadState;
        }
        void setThreadState(PyThreadState* threadState)
        {
            mThreadState = threadState;
        }
        
    private:
        PyInterpreterState* mInterpreterState;
        PyThreadState* mThreadState;
    };
    
    //--------------------------------------------------------------//
    PyExternalUser* _pyUser;
    boost::python::dict _context;
	
private:
	
};

template<class A>
void ModelPythonWrapper::py_call(std::string func, std::map<std::string, A> args)
{
    PyExternalUser::Use use(*_pyUser);
    try
    {
        try
        {
            boost::python::object f = boost::python::extract<boost::python::object>(_context[func.c_str()]);
            if(f)
            {
                boost::python::dict dictData;
                typedef typename std::map<std::string, A>::iterator itType; //use this instead!
                itType iter;
                for (iter = args.begin(); iter != args.end(); ++iter)
                {
                    dictData[iter->first] = iter->second;
                }
                
                f(dictData); //call the function !
            }
            else
            {
                std::cerr << "Script did not have a " << func << " function!\n";
            }
        }
        catch(const boost::python::error_already_set &e)
        {
            std::cerr << "Exception in script: ";
            ModelPythonWrapper::PythonContextManager::getInstance()->print_py_error();
        }
    }
    catch(const std::exception &e)
    {
        std::cerr << "Exception in script: " << e.what() << "\n";
    }
}

template<class T>
T ModelPythonWrapper::py_rcall(std::string func)
{
    T result;
    PyExternalUser::Use use(*_pyUser);
    try
    {
        try
        {
            boost::python::object f = boost::python::extract<boost::python::object>(_context[func.c_str()]);
            if(f)
            {
               result = boost::python::extract<T>(f()); //call the function !
            }
            else
            {
                std::cerr << "Script did not have a " << func << " function!\n";
            }
        }
        catch(const boost::python::error_already_set &e)
        {
            std::cerr << "Exception in script: ";
            ModelPythonWrapper::PythonContextManager::getInstance()->print_py_error();
        }
    }
    catch(const std::exception &e)
    {
        std::cerr << "Exception in script: " << e.what() << "\n";
    }
    
    return result;
}

template<class T, class A>
T ModelPythonWrapper::py_rcall(std::string func, std::map<std::string, A> args)
{
    T result;
    PyExternalUser::Use use(*_pyUser);
    try
    {
        try
        {
            boost::python::object f = boost::python::extract<boost::python::object>(_context[func.c_str()]);
            if(f)
            {
                boost::python::dict dictData;
                typedef typename std::map<std::string, A>::iterator itType;
                itType iter;
                for (iter = args.begin(); iter != args.end(); ++iter)
                {
                    dictData[iter->first] = iter->second;
                }
                
                result = boost::python::extract<T>(f(dictData)); //call the function !
            }
            else
            {
                std::cerr << "Script did not have a " << func << " function!\n";
            }
        }
        catch(const boost::python::error_already_set &e)
        {
            std::cerr << "Exception in script: ";
            ModelPythonWrapper::PythonContextManager::getInstance()->print_py_error();
        }
    }
    catch(const std::exception &e)
    {
        std::cerr << "Exception in script: " << e.what() << "\n";
    }
    
    return result;
}

/*
template<class T>
T ModelPythonWrapper::py_rcall(std::string func, std::map<std::string, float> args)
{
    T result;
    PyExternalUser::Use use(*_pyUser);
    try
    {
        try
        {
            boost::python::object f = boost::python::extract<boost::python::object>(_context[func.c_str()]);
            if(f)
            {
                boost::python::dict dictData;
                std::map<std::string, float>::iterator iter;
                for (iter = args.begin(); iter != args.end(); ++iter)
                {
                    dictData[iter->first] = iter->second;
                }
                
                result = boost::python::extract<T>(f(dictData)); //call the function !
            }
            else
            {
                std::cerr << "Script did not have a " << func << " function!\n";
            }
        }
        catch(const boost::python::error_already_set &e)
        {
            std::cerr << "Exception in script: ";
            ModelPythonWrapper::PythonContextManager::getInstance()->print_py_error();
        }
    }
    catch(const std::exception &e)
    {
        std::cerr << "Exception in script: " << e.what() << "\n";
    }
    
    return result;
}
*/

}
}

#endif
