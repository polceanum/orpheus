//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#ifndef ORPHEUS_MODEL_PERMISSIONS_H
#define ORPHEUS_MODEL_PERMISSIONS_H

#include <vector>
#include <string>
#include <map>
#include <boost/uuid/uuid.hpp>
#include <boost/shared_ptr.hpp>

namespace orpheus
{
namespace metamodel
{
class MentalImage;
}
namespace models
{

class ModelPermissions
{
public:
	ModelPermissions();
	virtual ~ModelPermissions();
	
    void grantAll(boost::shared_ptr<orpheus::metamodel::MentalImage>);
    void grantPermission(boost::uuids::uuid);
    void denyPermission(boost::uuids::uuid);
    void clearAll();
    
    bool hasPermission(boost::uuids::uuid);
    
    std::vector<boost::uuids::uuid> getPermissionList();
    
protected:
	
private:
    std::map<boost::uuids::uuid, bool> _allowedObjects;
};

}
}

#endif
