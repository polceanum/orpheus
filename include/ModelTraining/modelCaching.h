//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#ifndef ORPHEUS_MODEL_CACHING_H
#define ORPHEUS_MODEL_CACHING_H

#include "Models/model.h"

#include <boost/thread.hpp>
#include <boost/enable_shared_from_this.hpp>

namespace orpheus
{
namespace model_training
{

//     <<Singleton>>
class ModelCaching : public boost::enable_shared_from_this<ModelCaching>
{
public:
	virtual ~ModelCaching();
    
	static boost::shared_ptr<ModelCaching> getInstance();
    
    virtual void start();
	virtual void stop();
    
    void startCachingModel(std::size_t sessionId, std::size_t modelId, std::size_t nrOfCopies);
    void stopCachingModel(std::size_t sessionId, std::size_t modelId);
    
    boost::shared_ptr<orpheus::models::Model> getCachedModel(std::size_t sessionId, std::size_t modelId);
    
protected:
	
private:
    //----------------------------------------//
    class CacheData
    {
    public:
        std::size_t session;
        std::size_t model;
        std::size_t copies;
        std::vector<boost::shared_ptr<orpheus::models::Model> > cache;
    };
    //----------------------------------------//
    
	ModelCaching();
	static boost::shared_ptr<ModelCaching> _instance;
	
    virtual void worker(); //method that gets sent to the thread. all logic in here

	//main-only variables
	boost::shared_ptr<boost::thread> _thread;
	
	//cross-thread variables
	bool _working;
	boost::mutex _mutex;
    std::vector<boost::shared_ptr<CacheData> > _cacheData;
	
	//thread-only variables
    
};

}
}

#endif
