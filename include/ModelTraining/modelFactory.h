//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#ifndef ORPHEUS_MODELFACTORY
#define ORPHEUS_MODELFACTORY

#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>

#include "Models/model.h"

namespace orpheus
{
namespace model_training
{

//     <<Singleton>>
class ModelFactory
{
public:
	virtual ~ModelFactory();
	
	static boost::shared_ptr<ModelFactory> getInstance();
    
    boost::shared_ptr<orpheus::models::Model> createModel(std::string type);
	
protected:
	
private:
	ModelFactory();
	static boost::shared_ptr<ModelFactory> _instance;
	
    boost::mutex _sessionMutex;
    
    #ifdef __linux__
        std::map<std::string, void*> _handleMap;
    #elif _WIN32
        
    #endif
};

}
}

#endif
