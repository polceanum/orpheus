//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#ifndef ORPHEUS_MODELTRAINER
#define ORPHEUS_MODELTRAINER

#include <boost/shared_ptr.hpp>
#include <vector>
#include <map>

#include "Models/model.h"
#include "Metamodel/mentalImage.h"
#include "ModelTraining/trainingSession.h"
#include <boost/thread.hpp>

namespace orpheus
{
namespace model_training
{

//     <<Singleton>>
class ModelTrainer
{
public:
	virtual ~ModelTrainer();
	
	void addSession(boost::shared_ptr<TrainingSession>);
	void removeSession(std::size_t);
    void removeSession(boost::shared_ptr<TrainingSession>);
    void removeSession(boost::uuids::uuid);
    boost::shared_ptr<TrainingSession> accessSession(std::size_t);
    boost::shared_ptr<TrainingSession> accessSession(boost::uuids::uuid);
	std::size_t size();
    
    void addEvidence(boost::shared_ptr<orpheus::metamodel::MentalImage>, orpheus::value_specifications::ValueBag&);
    
    std::vector<boost::shared_ptr<orpheus::models::Model> > getBestTrainedModelSet();
    
	static boost::shared_ptr<ModelTrainer> getInstance();
	
protected:
	
private:
	ModelTrainer();
	static boost::shared_ptr<ModelTrainer> _instance;
	
    boost::mutex _sessionMutex;
	std::vector<boost::shared_ptr<TrainingSession> > _sessions;
    
    //evidence management
    boost::shared_ptr<boost::thread> _evidenceMan;
    boost::mutex _evidenceMutex;
    std::vector<boost::shared_ptr<orpheus::metamodel::MentalImage> > _evidence;
    std::vector<boost::shared_ptr<orpheus::metamodel::MentalImage> > _evidenceBuffer;
    std::vector<orpheus::value_specifications::ValueBag> _evidenceParams;
    std::vector<orpheus::value_specifications::ValueBag> _evidenceParamsBuffer;
    void _manageEvidence();
    
    //garbage collection
    boost::shared_ptr<boost::thread> _garbageMan;
    boost::mutex _garbageMutex;
    std::vector<boost::shared_ptr<TrainingSession> > _garbage;
    void _collectGarbage();
};

}
}

#endif
