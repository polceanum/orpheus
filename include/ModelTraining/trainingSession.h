//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#ifndef ORPHEUS_TRAINING_SESSION_H
#define ORPHEUS_TRAINING_SESSION_H

#include "Metamodel/mentalImage.h"
#include "Models/model.h"
#include <string>
#include <boost/thread.hpp>
#include <boost/enable_shared_from_this.hpp>

namespace orpheus
{
namespace model_training
{

class TrainingSession : public boost::enable_shared_from_this<TrainingSession>
{
public:
	TrainingSession();
	virtual ~TrainingSession();
	
	virtual void start();
	virtual void stop();
	virtual void destroy();
    
    void addModel(boost::shared_ptr<orpheus::models::Model>);
    
    std::vector<boost::shared_ptr<orpheus::models::Model> > getModelSet();
    
    boost::shared_ptr<orpheus::models::Model> getModel(std::size_t);
    
    void addEvidence(boost::shared_ptr<orpheus::metamodel::MentalImage>, const orpheus::value_specifications::ValueBag&);
    
    virtual void setStepSize(double);
    
    virtual bool isRunning(); //whether or not the thread is working
    
    double getCurrentError();
    
    void setId(boost::uuids::uuid);
    boost::uuids::uuid getId();
    
protected:
    
    boost::uuids::uuid _sessionId; //unique simulation identifier
    
    virtual void worker(); //method that gets sent to the thread. all logic in here

	//main-only variables
	boost::shared_ptr<boost::thread> _thread;
	
	//cross-thread variables
	bool _working;
	boost::mutex _mutex;
    double _stepSize;
    std::vector<boost::shared_ptr<orpheus::metamodel::MentalImage> > _evidenceQueue;
    std::vector<orpheus::value_specifications::ValueBag> _evidenceParamsQueue;
    double _currentError;
	
	//thread-only variables
	std::vector<boost::shared_ptr<orpheus::models::Model> > _models;
    bool _modelsInitalized;
    
private:
	
};

}
}

#endif
