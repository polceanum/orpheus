//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#ifndef ORPHEUS_MODELASSIGNMENT
#define ORPHEUS_MODELASSIGNMENT

#include <boost/shared_ptr.hpp>
#include <boost/random.hpp>
#include <vector>
#include <map>

#include "SimulationEngine/simulationLocal.h"
#include "Metamodel/mentalImage.h"

namespace orpheus
{
namespace model_assignment
{

//     <<Singleton>>
class ModelAssignment
{
public:
	virtual ~ModelAssignment();
    
    static boost::shared_ptr<ModelAssignment> getInstance();
    
    virtual void learnObjectModelAssociations(std::vector<boost::uuids::uuid>, boost::shared_ptr<orpheus::simulation_engine::Simulation>);
    virtual void assignModelsToSimulation(boost::shared_ptr<orpheus::simulation_engine::SimulationLocal>); //TODO: to be changed to generic Simulation
	
protected:
	
private:
    ModelAssignment();
    static boost::shared_ptr<ModelAssignment> _instance;
    
    std::map<boost::uuids::uuid, boost::shared_ptr<orpheus::models::Model> > _idToModel;
    
    boost::mt19937 _generator;

    double _dist()
    {
        boost::random::uniform_real_distribution<> dist(0.0, 1.0);
        return dist(_generator);
    }
    
};

}
}

#endif
