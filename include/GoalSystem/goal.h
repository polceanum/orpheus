//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#ifndef ORPHEUS_GOAL_H
#define ORPHEUS_GOAL_H

#include "Metamodel/mentalImage.h"

#include <string>

namespace orpheus
{
namespace goal_system
{

class Goal
{
public:
	Goal();
	virtual ~Goal();
	
    //----------------------------------------------------------------------//
    virtual void update(boost::shared_ptr<const orpheus::metamodel::MentalImage>) = 0;
    //----------------------------------------------------------------------//
    
    virtual bool isSatisfied() const;
    virtual void setSatisfactionThreshold(double);
    virtual void setSatisfaction(double);
    virtual double getSatisfaction() const;
    
    virtual bool requestsAbortSimulation() const;
    
	
protected:
    double _satisfaction;
    double _satisfactionThreshold;
    
    bool _abortSimulation;
    
private:
	
};

}
}

#endif
