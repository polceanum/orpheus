//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#ifndef ORPHEUS_TCP_CONNECTION_H
#define ORPHEUS_TCP_CONNECTION_H

#include <string>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/asio.hpp>
#include <boost/array.hpp>

namespace orpheus
{
namespace communication
{

class tcp_server;

#define BUFFER_SIZE 4096

class tcp_connection : public boost::enable_shared_from_this<tcp_connection>
{
public:

	virtual ~tcp_connection(void);

	static boost::shared_ptr<tcp_connection> create(boost::asio::io_service& io_service);

	boost::asio::ip::tcp::socket& socket();

	void start();
	
	void start_write(std::string msg);
	
	void start_async_write(std::string msg);
	
	void start_read();
    
    bool is_open();
	
	void close();
	
	void establish(std::string host, std::string port);
	
	std::string getLastReceivedMessage(); //must be thread safe
	
	void setMessagePost(boost::weak_ptr<tcp_server>);

private:
	tcp_connection(boost::asio::io_service& io_service);
	
	void handle_read(const boost::system::error_code& error, size_t bytes_transferred);
	void handle_write(const boost::system::error_code& error, size_t bytes_transferred);

	boost::asio::ip::tcp::socket _socket;
	boost::array<char, BUFFER_SIZE> _readBuffer;
	std::stringstream _receivedBulk;
	boost::mutex _mutex;
	std::string _lastReceivedMessage;
	boost::weak_ptr<tcp_server> _messagePost;
};

}
}

#endif
