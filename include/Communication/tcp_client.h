//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#ifndef ORPHEUS_TCP_CLIENT_H
#define ORPHEUS_TCP_CLIENT_H

#include <string>
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>

#include "Communication/tcp_connection.h"

namespace orpheus
{
namespace communication
{

class tcp_client
{
public:
	tcp_client(boost::asio::io_service& io_service);
	virtual ~tcp_client(void);
	
	void connect(std::string host, std::string port);
	
	void send(std::string msg);
	
	void async_send(std::string msg);
	
    bool is_connected();
    
    void reconnect();
    
	void disconnect();
    
    std::string getHost();
    std::string getPort();

private:
	boost::shared_ptr<tcp_connection> _connection;
    std::string _host;
    std::string _port;
};

}
}

#endif
