//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#ifndef ORPHEUS_TCP_SERVER_H
#define ORPHEUS_TCP_SERVER_H

#include <string>
#include <boost/bind.hpp>
#include <boost/thread/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/enable_shared_from_this.hpp>

#include "Communication/tcp_connection.h"
#include "Communication/request.h"

namespace orpheus
{
namespace communication
{

class CommunicationManager;

class tcp_server : public boost::enable_shared_from_this<tcp_server>
{
public:
	tcp_server(boost::asio::io_service& io_service, std::string port);
	virtual ~tcp_server(void);
	
	void postMessage(std::string); //synchronized, receives message which also contains expeditor
    
    void disconnectAllClients();
	
	std::string getLastMessage(); //TODO change this
	
	std::string getPort();
	
	void setManager(boost::weak_ptr<CommunicationManager>);

private:
	void start_accept();
	void handle_accept(boost::shared_ptr<tcp_connection> new_connection, const boost::system::error_code& error);
	
	void handle_request(boost::shared_ptr<request>);
	
	boost::weak_ptr<CommunicationManager> _manager;
	boost::asio::ip::tcp::acceptor _acceptor;
	boost::shared_ptr<boost::thread> _thread;
	std::vector<boost::weak_ptr<tcp_connection> > _connections;
	boost::mutex _messagePostMutex;
	//std::map<boost::weak_ptr<tcp_connection>, std::vector<std::string> > _mailBox;
	std::vector<std::string> _mailBox;
	std::string _port;
};

}
}

#endif
