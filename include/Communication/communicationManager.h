//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#ifndef ORPHEUS_COMMUNICATION_MANAGER_H
#define ORPHEUS_COMMUNICATION_MANAGER_H

#include <string>
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>

#include "Communication/tcp_client.h"
#include "Communication/tcp_server.h"

namespace orpheus
{
namespace communication
{

class CommunicationManager //multiton
{
public:
	virtual ~CommunicationManager(void);

	static boost::shared_ptr<CommunicationManager> getInstance(std::string aid);
	
	boost::shared_ptr<tcp_server> getServer();
	boost::shared_ptr<tcp_client> getClient(std::string host, std::string port, bool autoConnect=true);
    void destroyClient(boost::shared_ptr<tcp_client>);
	
private:
	CommunicationManager();
	
	static std::map<std::string, boost::shared_ptr<CommunicationManager> > _instances;
	static boost::asio::io_service _io_service;
	
	boost::shared_ptr<tcp_server> _server;
	std::map<std::string, boost::shared_ptr<tcp_client> > _clients;
};

}
}

#endif
