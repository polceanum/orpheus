//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#ifndef VALUE_SPECIFICATION_H
#define VALUE_SPECIFICATION_H

#include <string>
#include "boost/shared_ptr.hpp"

namespace orpheus
{
namespace value_specifications
{

class ValueSpecification
{
public:
    ValueSpecification();
    virtual ~ValueSpecification();
    
    virtual std::string asString() const = 0;
    virtual bool asBool() const;
    virtual int asInt() const;
    virtual long long asLongLong() const;
    virtual float asFloat() const;
    virtual double asDouble() const;
    //etc...
    
    virtual boost::shared_ptr<ValueSpecification> copy() const = 0;
    
    static boost::shared_ptr<ValueSpecification> Bool(bool);
    static boost::shared_ptr<ValueSpecification> Int(int);
    static boost::shared_ptr<ValueSpecification> LongLong(long long);
    static boost::shared_ptr<ValueSpecification> Float(float);
    static boost::shared_ptr<ValueSpecification> Double(double);
    static boost::shared_ptr<ValueSpecification> String(std::string);
    
    //serialization purpose
    std::string toString() const;
    static boost::shared_ptr<ValueSpecification> fromString(const std::string &);
    
protected:
    std::string _type;
    
private:
    
};

}
}

#endif
