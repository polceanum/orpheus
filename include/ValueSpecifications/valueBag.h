//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#ifndef VALUE_BAG_H
#define VALUE_BAG_H

#include <string>
#include <vector>
#include <map>
#include "boost/shared_ptr.hpp"

namespace orpheus
{
namespace value_specifications
{
class ValueSpecification;

class ValueBag
{
public:
    ValueBag();
    virtual ~ValueBag();
    
    std::map<std::string, boost::shared_ptr<ValueSpecification> >& content();
    bool contains(std::string) const;
    boost::shared_ptr<const ValueSpecification> get(std::string) const;
    
    ValueBag copy() const;
    
    void setValuesFrom(const ValueBag&);
    
    //serialization purpose
    std::string toString();
    static ValueBag fromString(std::string);
    
protected:
    std::map<std::string, boost::shared_ptr<ValueSpecification> > _contents;
    
private:
    
};

}
}

#endif
