//-------------------------------------------------------------------------//
// This file is a part of  ORPHEUS:                                        //
//                         Reasoning and                                   //
//                         Prediction with                                 //
//                         Heterogeneous                                   //
//                        rEpresentations                                  //
//                         Using                                           //
//                         Simulation                                      //
//                                                                         //
// Copyright (C) 2013-2015 Mihai Polceanu <polceanu@enib.fr>               //
// Copyright (C) 2013-2015 CERV: European Center for Virtual Reality, FR   //
//                                                                         //
// ORPHEUS is free software; you can redistribute it and/or                //
// modify it under the terms of the GNU Lesser General Public              //
// License as published by the Free Software Foundation; either            //
// version 2.1 of the License, or (at your option) any later version.      //
//                                                                         //
// ORPHEUS is distributed in the hope that it will be useful,              //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        //
// Lesser General Public License for more details.                         //
//                                                                         //
// You should have received a copy of the GNU Lesser General Public        //
// License along with ORPHEUS; if not, see <http://www.gnu.org/licenses/>. //
//-------------------------------------------------------------------------//

#ifndef DOUBLE_SPECIFICATION_H
#define DOUBLE_SPECIFICATION_H

#include <string>
#include "boost/shared_ptr.hpp"

#include "ValueSpecifications/valueSpecification.h"

namespace orpheus
{
namespace value_specifications
{

class DoubleSpecification : public ValueSpecification
{
public:
    DoubleSpecification(double val=0.0);
    virtual ~DoubleSpecification();
    
    virtual std::string asString() const;
    virtual double asDouble() const; //direct method
    
    virtual boost::shared_ptr<ValueSpecification> copy() const;
    
protected:
    
private:
    double _value;
    
};

}
}

#endif
