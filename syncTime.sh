#! /bin/sh
echo "Stopping NTP service"
sudo service ntp stop
echo "Forcing synchronization with 192.168.40.103"
sudo ntpd -q
echo "Starting NTP service"
sudo service ntp start
